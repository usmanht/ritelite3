<?php
$module_name = 'RL_SalesHistory';
$listViewDefs [$module_name] = 
array (
  'SOP_NUMBER' => 
  array (
    'type' => 'int',
    'label' => 'LBL_SOP_NUMBER',
    'width' => '10%',
    'default' => true,
  ),
  'INVOICE_NUMBER' => 
  array (
    'type' => 'int',
    'label' => 'LBL_INVOICE_NUMBER',
    'width' => '10%',
    'default' => true,
  ),
  'CUSTOMER_ORDER_NUMBER' => 
  array (
    'type' => 'int',
    'label' => 'LBL_CUSTOMER_ORDER_NUMBER',
    'width' => '10%',
    'default' => true,
  ),
  'ACCREF' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_ACCREF',
    'width' => '10%',
    'default' => true,
  ),
  'CARRIAGE_RATE_CHARGED' => 
  array (
    'type' => 'currency',
    'label' => 'LBL_CARRIAGE_RATE_CHARGED',
    'currency_format' => true,
    'width' => '10%',
    'default' => true,
  ),
  'ORDER_DATE' => 
  array (
    'type' => 'date',
    'label' => 'LBL_ORDER_DATE',
    'width' => '10%',
    'default' => true,
  ),
  'DESPATCH_DATE' => 
  array (
    'type' => 'date',
    'label' => 'LBL_DESPATCH_DATE',
    'width' => '10%',
    'default' => true,
  ),
  'TOTAL_VALUE' => 
  array (
    'type' => 'currency',
    'label' => 'LBL_TOTAL_VALUE',
    'currency_format' => true,
    'width' => '10%',
    'default' => true,
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '9%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => false,
  ),
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => false,
    'link' => true,
  ),
);
?>
