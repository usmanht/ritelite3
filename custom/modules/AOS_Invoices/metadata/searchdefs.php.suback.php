<?php
// created: 2016-03-04 15:10:16
$searchdefs['AOS_Invoices'] = array (
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
    'maxColumnsBasic' => '3',
  ),
  'layout' => 
  array (
    'basic_search' => 
    array (
      0 => 
      array (
        'type' => 'int',
        'label' => 'LBL_INVOICE_NUMBER',
        'default' => true,
        'width' => '10%',
        'name' => 'number',
      ),
      1 => 
      array (
        'type' => 'varchar',
        'default' => true,
        'label' => 'LBL_SAGE_SOP_NUMBER',
        'width' => '10%',
        'name' => 'sage_sop_number_c',
      ),
      2 => 
      array (
        'type' => 'varchar',
        'default' => true,
        'label' => 'LBL_CUSTOMER_ORDER_NUMBER',
        'width' => '10%',
        'name' => 'customer_order_number_c',
      ),
      3 => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      4 => 
      array (
        'type' => 'relate',
        'studio' => 'visible',
        'label' => 'LBL_BILLING_ACCOUNT',
        'id' => 'BILLING_ACCOUNT_ID',
        'link' => true,
        'default' => true,
        'width' => '10%',
        'name' => 'billing_account',
      ),
      5 => 
      array (
        'type' => 'relate',
        'link' => true,
        'label' => 'LBL_ACCOUNTS_AOS_INVOICES_1_FROM_ACCOUNTS_TITLE',
        'id' => 'ACCOUNTS_AOS_INVOICES_1ACCOUNTS_IDA',
        'width' => '10%',
        'default' => true,
        'name' => 'accounts_aos_invoices_1_name',
      ),
      6 => 
      array (
        'type' => 'int',
        'label' => 'LBL_QUOTE_NUMBER',
        'width' => '10%',
        'default' => true,
        'name' => 'quote_number',
      ),
      7 => 
      array (
        'type' => 'int',
        'default' => true,
        'label' => 'LBL_INVOICE_NO',
        'width' => '10%',
        'name' => 'invoice_no_c',
      ),
      8 => 
      array (
        'name' => 'current_user_only',
        'label' => 'LBL_CURRENT_USER_FILTER',
        'type' => 'bool',
        'default' => true,
        'width' => '10%',
      ),
      9 => 
      array (
        'type' => 'enum',
        'label' => 'LBL_ASSIGNED_TO',
        'id' => 'ASSIGNED_USER_ID',
        'link' => true,
        'name' => 'assigned_user_id',
        'function' => 
        array (
          'name' => 'get_user_array',
          'params' => 
          array (
            0 => false,
          ),
        ),
        'width' => '10%',
        'default' => true,
      ),
    ),
    'advanced_search' => 
    array (
      0 => 
      array (
        'name' => 'number',
        'default' => true,
        'width' => '10%',
      ),
      1 => 
      array (
        'type' => 'varchar',
        'default' => true,
        'label' => 'LBL_SAGE_SOP_NUMBER',
        'width' => '10%',
        'name' => 'sage_sop_number_c',
      ),
      2 => 
      array (
        'type' => 'varchar',
        'default' => true,
        'label' => 'LBL_CUSTOMER_ORDER_NUMBER',
        'width' => '10%',
        'name' => 'customer_order_number_c',
      ),
      3 => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      4 => 
      array (
        'name' => 'billing_account',
        'default' => true,
        'width' => '10%',
      ),
      5 => 
      array (
        'name' => 'billing_contact',
        'default' => true,
        'width' => '10%',
      ),
      6 => 
      array (
        'type' => 'relate',
        'link' => true,
        'label' => 'LBL_ACCOUNTS_AOS_INVOICES_1_FROM_ACCOUNTS_TITLE',
        'id' => 'ACCOUNTS_AOS_INVOICES_1ACCOUNTS_IDA',
        'width' => '10%',
        'default' => true,
        'name' => 'accounts_aos_invoices_1_name',
      ),
      7 => 
      array (
        'type' => 'relate',
        'link' => true,
        'label' => 'LBL_CONTACTS_AOS_INVOICES_1_FROM_CONTACTS_TITLE',
        'id' => 'CONTACTS_AOS_INVOICES_1CONTACTS_IDA',
        'width' => '10%',
        'default' => true,
        'name' => 'contacts_aos_invoices_1_name',
      ),
      8 => 
      array (
        'name' => 'total_amount',
        'default' => true,
        'width' => '10%',
      ),
      9 => 
      array (
        'type' => 'int',
        'label' => 'LBL_QUOTE_NUMBER',
        'width' => '10%',
        'default' => true,
        'name' => 'quote_number',
      ),
      10 => 
      array (
        'label' => 'LBL_CURRENT_USER_FILTER',
        'type' => 'bool',
        'default' => true,
        'width' => '10%',
        'name' => 'current_user_only',
      ),
      11 => 
      array (
        'type' => 'datetime',
        'label' => 'LBL_DATE_ENTERED',
        'width' => '10%',
        'default' => true,
        'name' => 'date_entered',
      ),
      12 => 
      array (
        'type' => 'date',
        'label' => 'LBL_QUOTE_DATE',
        'width' => '10%',
        'default' => true,
        'name' => 'quote_date',
      ),
      13 => 
      array (
        'name' => 'assigned_user_id',
        'type' => 'enum',
        'label' => 'LBL_ASSIGNED_TO',
        'function' => 
        array (
          'name' => 'get_user_array',
          'params' => 
          array (
            0 => false,
          ),
        ),
        'default' => true,
        'width' => '10%',
      ),
      14 => 
      array (
        'name' => 'status',
        'default' => true,
        'width' => '10%',
      ),
      15 => 
      array (
        'type' => 'enum',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_SALES_ORDER_STATUS',
        'width' => '10%',
        'name' => 'sales_order_status_c',
      ),
      16 => 
      array (
        'type' => 'assigned_user_name',
        'label' => 'LBL_CREATED',
        'width' => '10%',
        'default' => true,
        'name' => 'created_by',
      ),
    ),
  ),
);