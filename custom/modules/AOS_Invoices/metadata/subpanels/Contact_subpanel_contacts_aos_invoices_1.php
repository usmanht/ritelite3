<?php
// created: 2014-05-08 21:16:01
$subpanel_layout['list_fields'] = array (
  'date_entered' => 
  array (
    'type' => 'datetime',
    'vname' => 'LBL_DATE_ENTERED',
    'width' => '5%',
    'default' => true,
  ),
  'number' => 
  array (
    'width' => '5%',
    'vname' => 'LBL_LIST_NUM',
    'default' => true,
  ),
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '10%',
    'default' => true,
  ),
  'customer_order_number_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_CUSTOMER_ORDER_NUMBER',
    'width' => '5%',
  ),
  'sage_sop_number_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_SAGE_SOP_NUMBER',
    'width' => '5%',
  ),
  'quote_number' => 
  array (
    'type' => 'int',
    'vname' => 'LBL_QUOTE_NUMBER',
    'width' => '5%',
    'default' => true,
  ),
  'contacts_aos_invoices_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_CONTACTS_AOS_INVOICES_1_FROM_CONTACTS_TITLE',
    'id' => 'CONTACTS_AOS_INVOICES_1CONTACTS_IDA',
    'width' => '10%',
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Contacts',
    'target_record_key' => 'contacts_aos_invoices_1contacts_ida',
  ),
  'billing_account' => 
  array (
    'width' => '10%',
    'vname' => 'LBL_BILLING_ACCOUNT',
    'default' => true,
  ),
  'total_amount' => 
  array (
    'type' => 'currency',
    'vname' => 'LBL_GRAND_TOTAL',
    'currency_format' => true,
    'width' => '5%',
    'default' => true,
  ),
  'due_date' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_DUE_DATE',
    'width' => '5%',
    'default' => true,
  ),
  'invoice_no_c' => 
  array (
    'type' => 'int',
    'default' => true,
    'vname' => 'LBL_INVOICE_NO',
    'width' => '5%',
  ),
  'status' => 
  array (
    'width' => '5%',
    'vname' => 'LBL_STATUS',
    'default' => true,
  ),
  'assigned_user_name' => 
  array (
    'name' => 'assigned_user_name',
    'vname' => 'LBL_ASSIGNED_USER',
    'width' => '10%',
    'default' => true,
  ),
  'edit_button' => 
  array (
    'widget_class' => 'SubPanelEditButton',
    'module' => 'AOS_Invoices',
    'width' => '4%',
    'default' => true,
  ),
  'remove_button' => 
  array (
    'widget_class' => 'SubPanelRemoveButton',
    'module' => 'AOS_Invoices',
    'width' => '4%',
    'default' => true,
  ),
  'currency_id' => 
  array (
    'usage' => 'query_only',
  ),
);