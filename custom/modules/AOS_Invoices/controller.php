<?php
/**
 * Products, Quotations & Invoices modules.
 * Extensions to SugarCRM
 * @package Advanced OpenSales for SugarCRM
 * @subpackage Products
 * @copyright SalesAgility Ltd http://www.salesagility.com
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU AFFERO GENERAL PUBLIC LICENSE
 * along with this program; if not, see http://www.gnu.org/licenses
 * or write to the Free Software Foundation,Inc., 51 Franklin Street,
 * Fifth Floor, Boston, MA 02110-1301  USA
 *
 * @author Salesagility Ltd <support@salesagility.com>
 */
 
require_once('include/MVC/Controller/SugarController.php');

class AOS_InvoicesController extends SugarController {
	function action_editview() {
		global $mod_string;

		$this->view = 'edit';
		$GLOBALS['view'] = $this->view;
		
        	if (isset($_REQUEST['aos_quotes_id'])) {
            		$query = "SELECT * FROM aos_quotes WHERE id = '{$_REQUEST['aos_quotes_id']}'";
            		$result = $this->bean->db->query($query, true);
            		$row = $this->bean->db->fetchByAssoc($result);
            		$this->bean->name = $row['name'];
            
			if (isset($row['billing_account_id'])) {
			$_REQUEST['account_id'] = $row['billing_account_id'];
        		}
        		
        		if (isset($row['billing_contact_id'])) {
        		$_REQUEST['contact_id'] = $row['billing_contact_id'];
        		}
        	}


		if (isset($_REQUEST['account_id'])) {
            		$query = "SELECT * FROM accounts INNER JOIN accounts_cstm ON accounts_cstm.id_c = accounts.id WHERE id = '{$_REQUEST['account_id']}'";
			$result = $this->bean->db->query($query, true);
			$row = $this->bean->db->fetchByAssoc($result);
            //flag set to check when billing contact to be left blank
            $blling_contact_flag = 0;

            if($this->IsNullOrEmptyString($row['parent_id'])){

                $this->bean->billing_account_id = $row['id'];
                $this->bean->billing_account = $row['name'];
                $this->bean->billing_address_street = $row['billing_address_street'];
                $this->bean->billing_address_city = $row['billing_address_city'];
                $this->bean->billing_address_state = $row['billing_address_state'];
                $this->bean->billing_address_postalcode = $row['billing_address_postalcode'];
                $this->bean->billing_address_country = $row['billing_address_country'];
                $this->bean->sage_ref_billing = $row['sage_account_ref_c'];

                //shipping
                $this->bean->accounts_aos_invoices_1accounts_ida = $row['id'];
                $this->bean->accounts_aos_invoices_1_name = $row['name'];
                $this->bean->shipping_address_street = $row['shipping_address_street'];
                $this->bean->shipping_address_city = $row['shipping_address_city'];
                $this->bean->shipping_address_state = $row['shipping_address_state'];
                $this->bean->shipping_address_postalcode = $row['shipping_address_postalcode'];
                $this->bean->shipping_address_country = $row['shipping_address_country'];
                $this->bean->sage_ref_shipping = $row['sage_account_ref_c'];

            }
            else {
                $query2 = "SELECT * FROM accounts INNER JOIN accounts_cstm ON accounts_cstm.id_c = accounts.id WHERE id = '{$row['parent_id']}'";

                $blling_contact_flag = 1;

                $result2 = $this->bean->db->query($query2, true);
                $row2 = $this->bean->db->fetchByAssoc($result2);

                $this->bean->billing_account_id = $row2['id'];
                $this->bean->billing_account = $row2['name'];
                $this->bean->billing_address_street = $row2['billing_address_street'];
                $this->bean->billing_address_city = $row2['billing_address_city'];
                $this->bean->billing_address_state = $row2['billing_address_state'];
                $this->bean->billing_address_postalcode = $row2['billing_address_postalcode'];
                $this->bean->billing_address_country = $row2['billing_address_country'];
                $this->bean->sage_ref_billing = $row2['sage_account_ref_c'];

                //shipping
                $this->bean->accounts_aos_invoices_1accounts_ida = $row['id'];
                $this->bean->accounts_aos_invoices_1_name = $row['name'];
                $this->bean->shipping_address_street = $row['shipping_address_street'];
                $this->bean->shipping_address_city = $row['shipping_address_city'];
                $this->bean->shipping_address_state = $row['shipping_address_state'];
                $this->bean->shipping_address_postalcode = $row['shipping_address_postalcode'];
                $this->bean->shipping_address_country = $row['shipping_address_country'];
                $this->bean->sage_ref_shipping = $row['sage_account_ref_c'];


            }
		}	
		
		if (isset($_REQUEST['contact_id'])) {
            		$query = "SELECT id,first_name,last_name FROM contacts WHERE id = '{$_REQUEST['contact_id']}'";
			$result = $this->bean->db->query($query, true);
			$row = $this->bean->db->fetchByAssoc($result);
            //if 1 leave blank
            if($blling_contact_flag == 1){

                $this->bean->billing_contact_id = '';
                $this->bean->billing_contact = '';
            }
            else {
                $this->bean->billing_contact_id = $row['id'];
                $this->bean->billing_contact = $row['first_name'].' '.$row['last_name'];
            }

            $this->bean->contacts_aos_invoices_1contacts_ida = $row['id'];
            $this->bean->contacts_aos_invoices_1_name = $row['first_name'].' '.$row['last_name'];
		}
        	
    }

    //Function for basic field validation (present and neither empty nor only white space)
    function IsNullOrEmptyString($question){
        return (!isset($question) || trim($question)==='');
    }

    /*
     *  Function to do custom save (this may not work) with the intention of overriding the AOS_Line_Item_Groups.php file
     */

    function action_save(){
        global $sugar_config;

        if (empty($this->id)  || $this->new_with_id){
            if(isset($_POST['group_id'])) unset($_POST['group_id']);
            if(isset($_POST['product_id'])) unset($_POST['product_id']);
            if(isset($_POST['service_id'])) unset($_POST['service_id']);

            if($sugar_config['dbconfig']['db_type'] == 'mssql'){
                //$this->number = $this->db->getOne("SELECT MAX(CAST(number as INT))+1 FROM aos_invoices");
                $this->number = $this->bean->db->getOne("SELECT MAX(CAST(number as INT))+1 FROM aos_invoices");
            } else {
                $this->number = $this->bean->db->getOne("SELECT MAX(CAST(number as UNSIGNED))+1 FROM aos_invoices");
            }

            if($this->number < $sugar_config['aos']['invoices']['initialNumber']){
                $this->number = $sugar_config['aos']['invoices']['initialNumber'];
            }
        }

        require_once('modules/AOS_Products_Quotes/AOS_Utils.php');

        perform_aos_save($this);

       // parent::save($check_notify);
          $this->bean->save();

        $this->save_groups($_REQUEST, $this->bean, 'group_');

      }


    function save_groups($post_data, $parent, $key = ''){

        $groups = array();
        $group_count = count($post_data[$key.'group_number']);
        $j = 0;
        for ($i = 0; $i < $group_count; ++$i) {

            if($post_data[$key.'deleted'][$i] == 1) {
                $this->mark_deleted($post_data[$key.'id'][$i]);
            } else {
                if(empty($post_data[$key.'id'][$i])) {
                    $product_quote_group = BeanFactory::newBean('AOS_Line_Item_Groups');
                } else {
                    $product_quote_group = BeanFactory::getBean('AOS_Line_Item_Groups', $post_data[$key.'id'][$i]);
                }
                foreach($product_quote_group->field_defs as $field_def) {
                    if(isset($post_data[$key.$field_def['name']][$i])){
                        $product_quote_group->$field_def['name'] = $post_data[$key.$field_def['name']][$i];
                    }
                }
                $product_quote_group->number = ++$j;
                $product_quote_group->assigned_user_id = $parent->assigned_user_id;
                $product_quote_group->parent_id = $parent->id;
                $product_quote_group->parent_type = $parent->object_name;
                $product_quote_group->save();

                if(isset($post_data[$key.'group_number'][$i])){
                    $groups[$post_data[$key.'group_number'][$i]] = $product_quote_group->id;
                }

            }
        }

        if($parent->object_name == 'AOS_Invoices'){
            require_once('modules/RL_Sales_Order_Line_Items/RL_Sales_Order_Line_Items.php');
            $productQuote = BeanFactory::newBean('RL_Sales_Order_Line_Items');
            $productQuote->save_lines($post_data, $parent, $groups, 'product_');
            $productQuote->save_lines($post_data, $parent, $groups, 'service_');

        }
        else {
            require_once('modules/AOS_Products_Quotes/AOS_Products_Quotes.php');
            $productQuote = new AOS_Products_Quotes();
            $productQuote->save_lines($post_data, $parent, $groups, 'product_');
            $productQuote->save_lines($post_data, $parent, $groups, 'service_');
        }

    }
}

?> 
