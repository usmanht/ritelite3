<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2014-05-07 16:14:02
$dictionary["AOS_Invoices"]["fields"]["accounts_aos_invoices_1"] = array (
  'name' => 'accounts_aos_invoices_1',
  'type' => 'link',
  'relationship' => 'accounts_aos_invoices_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_ACCOUNTS_AOS_INVOICES_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_aos_invoices_1accounts_ida',
);
$dictionary["AOS_Invoices"]["fields"]["accounts_aos_invoices_1_name"] = array (
  'name' => 'accounts_aos_invoices_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_AOS_INVOICES_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_aos_invoices_1accounts_ida',
  'link' => 'accounts_aos_invoices_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["AOS_Invoices"]["fields"]["accounts_aos_invoices_1accounts_ida"] = array (
  'name' => 'accounts_aos_invoices_1accounts_ida',
  'type' => 'link',
  'relationship' => 'accounts_aos_invoices_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_AOS_INVOICES_1_FROM_AOS_INVOICES_TITLE',
);


// created: 2015-01-12 15:28:24
$dictionary["AOS_Invoices"]["fields"]["aos_invoices_securitygroups_1"] = array (
  'name' => 'aos_invoices_securitygroups_1',
  'type' => 'link',
  'relationship' => 'aos_invoices_securitygroups_1',
  'source' => 'non-db',
  'module' => 'SecurityGroups',
  'bean_name' => 'SecurityGroup',
  'vname' => 'LBL_AOS_INVOICES_SECURITYGROUPS_1_FROM_SECURITYGROUPS_TITLE',
);


// created: 2014-05-08 10:48:41
$dictionary["AOS_Invoices"]["fields"]["aos_products_aos_invoices_1"] = array (
  'name' => 'aos_products_aos_invoices_1',
  'type' => 'link',
  'relationship' => 'aos_products_aos_invoices_1',
  'source' => 'non-db',
  'module' => 'AOS_Products',
  'bean_name' => 'AOS_Products',
  'vname' => 'LBL_AOS_PRODUCTS_AOS_INVOICES_1_FROM_AOS_PRODUCTS_TITLE',
);


// created: 2014-05-07 16:27:46
$dictionary["AOS_Invoices"]["fields"]["contacts_aos_invoices_1"] = array (
  'name' => 'contacts_aos_invoices_1',
  'type' => 'link',
  'relationship' => 'contacts_aos_invoices_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_CONTACTS_AOS_INVOICES_1_FROM_CONTACTS_TITLE',
  'id_name' => 'contacts_aos_invoices_1contacts_ida',
);
$dictionary["AOS_Invoices"]["fields"]["contacts_aos_invoices_1_name"] = array (
  'name' => 'contacts_aos_invoices_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CONTACTS_AOS_INVOICES_1_FROM_CONTACTS_TITLE',
  'save' => true,
  'id_name' => 'contacts_aos_invoices_1contacts_ida',
  'link' => 'contacts_aos_invoices_1',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["AOS_Invoices"]["fields"]["contacts_aos_invoices_1contacts_ida"] = array (
  'name' => 'contacts_aos_invoices_1contacts_ida',
  'type' => 'link',
  'relationship' => 'contacts_aos_invoices_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_AOS_INVOICES_1_FROM_AOS_INVOICES_TITLE',
);


// created: 2014-11-12 17:42:29
$dictionary["AOS_Invoices"]["fields"]["documents_aos_invoices_1"] = array (
  'name' => 'documents_aos_invoices_1',
  'type' => 'link',
  'relationship' => 'documents_aos_invoices_1',
  'source' => 'non-db',
  'module' => 'Documents',
  'bean_name' => 'Document',
  'vname' => 'LBL_DOCUMENTS_AOS_INVOICES_1_FROM_DOCUMENTS_TITLE',
);


// created: 2014-08-19 18:08:45
$dictionary["AOS_Invoices"]["fields"]["opportunities_aos_invoices_1"] = array (
  'name' => 'opportunities_aos_invoices_1',
  'type' => 'link',
  'relationship' => 'opportunities_aos_invoices_1',
  'source' => 'non-db',
  'module' => 'Opportunities',
  'bean_name' => 'Opportunity',
  'vname' => 'LBL_OPPORTUNITIES_AOS_INVOICES_1_FROM_OPPORTUNITIES_TITLE',
  'id_name' => 'opportunities_aos_invoices_1opportunities_ida',
);
$dictionary["AOS_Invoices"]["fields"]["opportunities_aos_invoices_1_name"] = array (
  'name' => 'opportunities_aos_invoices_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_OPPORTUNITIES_AOS_INVOICES_1_FROM_OPPORTUNITIES_TITLE',
  'save' => true,
  'id_name' => 'opportunities_aos_invoices_1opportunities_ida',
  'link' => 'opportunities_aos_invoices_1',
  'table' => 'opportunities',
  'module' => 'Opportunities',
  'rname' => 'name',
);
$dictionary["AOS_Invoices"]["fields"]["opportunities_aos_invoices_1opportunities_ida"] = array (
  'name' => 'opportunities_aos_invoices_1opportunities_ida',
  'type' => 'link',
  'relationship' => 'opportunities_aos_invoices_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_OPPORTUNITIES_AOS_INVOICES_1_FROM_AOS_INVOICES_TITLE',
);


$dictionary["AOS_Invoices"]["fields"]["sage_ref_billing"] = array(
    'name' => 'sage_ref_billing',
    'vname' => 'LBL_SAGE_REF',
    'comments' => '',
    'help' => '',
    'module' => 'AOS_Invoices',
    'type' => 'varchar',
    'max_size' => '155',
    'reportable' => '0',
   // 'source' => 'non-db',
    //'dbtype' => 'non-db',
    'studio' => 'visible'
);

$dictionary["AOS_Invoices"]["fields"]["sage_ref_shipping"] = array(
    'name' => 'sage_ref_shipping',
    'vname' => 'LBL_SAGE_REF1',
    'comments' => '',
    'help' => '',
    'module' => 'AOS_Invoices',
    'type' => 'varchar',
    'max_size' => '155',
    'reportable' => '0',
   // 'source' => 'non-db',
    //'dbtype' => 'non-db',
    'studio' => 'visible'
);

 // created: 2016-07-25 14:45:52
$dictionary['AOS_Invoices']['fields']['autosalesorderconfsend_c']['inline_edit']='1';
$dictionary['AOS_Invoices']['fields']['autosalesorderconfsend_c']['labelValue']='Automatically send Sales Order Confirmation by Email';

 

 // created: 2014-06-12 14:50:22
$dictionary['AOS_Invoices']['fields']['carrier_c']['labelValue']='Carrier';

 

 // created: 2016-07-26 09:39:51
$dictionary['AOS_Invoices']['fields']['confirmed_despatch_date_c']['inline_edit']='1';
$dictionary['AOS_Invoices']['fields']['confirmed_despatch_date_c']['options']='date_range_search_dom';
$dictionary['AOS_Invoices']['fields']['confirmed_despatch_date_c']['labelValue']='Confirmed Despatch Date';
$dictionary['AOS_Invoices']['fields']['confirmed_despatch_date_c']['enable_range_search']='1';

 

 // created: 2014-05-07 13:50:14
$dictionary['AOS_Invoices']['fields']['customer_order_number_c']['labelValue']='Customer Order Number';

 

 // created: 2014-05-07 13:50:14
$dictionary['AOS_Invoices']['fields']['date_despatched_c']['options']='date_range_search_dom';
$dictionary['AOS_Invoices']['fields']['date_despatched_c']['labelValue']='Date Despatched';
$dictionary['AOS_Invoices']['fields']['date_despatched_c']['enable_range_search']='1';

 

 // created: 2014-05-07 12:38:54
$dictionary['AOS_Invoices']['fields']['due_date']['merge_filter']='disabled';

 

 // created: 2014-05-07 13:50:15
$dictionary['AOS_Invoices']['fields']['export_docs_req_c']['labelValue']='Export Documentation Required';

 

 // created: 2015-05-28 13:02:11
$dictionary['AOS_Invoices']['fields']['export_docs_title_c']['labelValue']='export docs title';

 

$dictionary['AOS_Invoices']['fields']['invoice_date']['display_default']='';
$dictionary['AOS_Invoices']['fields']['invoice_date']['merge_filter']='disabled';

 

 // created: 2014-05-07 13:50:15
$dictionary['AOS_Invoices']['fields']['invoice_no_c']['options']='numeric_range_search_dom';
$dictionary['AOS_Invoices']['fields']['invoice_no_c']['labelValue']='Invoice Number';
$dictionary['AOS_Invoices']['fields']['invoice_no_c']['enable_range_search']='1';

 

 // created: 2015-05-28 15:49:21
$dictionary['AOS_Invoices']['fields']['invoice_number_c']['labelValue']='invoice number';

 

 // created: 2015-05-28 15:52:28
$dictionary['AOS_Invoices']['fields']['invoice_number_export_docs_c']['labelValue']='invoice number export docs';

 

 // created: 2016-08-10 10:44:57
$dictionary['AOS_Invoices']['fields']['notes_despatch_date_conf_c']['inline_edit']='1';
$dictionary['AOS_Invoices']['fields']['notes_despatch_date_conf_c']['labelValue']='Notes: Confirmed Despatch Date';

 

 // created: 2015-05-28 13:13:20
$dictionary['AOS_Invoices']['fields']['notes_export_docs_c']['labelValue']='Notes Export Docs';

 

 // created: 2015-05-28 14:00:06
$dictionary['AOS_Invoices']['fields']['parcels_weights_dims_c']['labelValue']='Parcels, weights & dimensions';

 

 // created: 2014-05-07 13:50:15
$dictionary['AOS_Invoices']['fields']['payment_method_list_c']['labelValue']='Payment Method';

 

 // created: 2014-05-07 13:50:15
$dictionary['AOS_Invoices']['fields']['payment_terms_c']['labelValue']='Payment Terms';

 

 // created: 2016-07-25 14:39:20
$dictionary['AOS_Invoices']['fields']['productiondespatchdateconf_c']['inline_edit']='1';
$dictionary['AOS_Invoices']['fields']['productiondespatchdateconf_c']['labelValue']='Request Despatch Date Confirmation from Production';

 

 // created: 2014-04-22 13:56:43
$dictionary['AOS_Invoices']['fields']['quote_date']['merge_filter']='disabled';

 

 // created: 2014-04-23 11:03:01
$dictionary['AOS_Invoices']['fields']['quote_number']['options']='numeric_range_search_dom';
$dictionary['AOS_Invoices']['fields']['quote_number']['merge_filter']='disabled';
$dictionary['AOS_Invoices']['fields']['quote_number']['enable_range_search']='1';
$dictionary['AOS_Invoices']['fields']['quote_number']['min']=false;
$dictionary['AOS_Invoices']['fields']['quote_number']['max']=false;
$dictionary['AOS_Invoices']['fields']['quote_number']['disable_num_format']='1';

 

 // created: 2015-11-23 10:11:50
$dictionary['AOS_Invoices']['fields']['requested_delivery_date_c']['labelValue']='Requested Delivery Date';

 

 // created: 2015-11-23 10:12:06
$dictionary['AOS_Invoices']['fields']['requried_despatch_date_c']['labelValue']='Requried Despatch Date';

 

 // created: 2014-08-01 11:34:43
$dictionary['AOS_Invoices']['fields']['sage_ref_billing']['len']='155';
$dictionary['AOS_Invoices']['fields']['sage_ref_billing']['merge_filter']='disabled';
$dictionary['AOS_Invoices']['fields']['sage_ref_billing']['reportable']=true;

 

 // created: 2014-08-01 11:35:33
$dictionary['AOS_Invoices']['fields']['sage_ref_shipping']['len']='155';
$dictionary['AOS_Invoices']['fields']['sage_ref_shipping']['merge_filter']='disabled';
$dictionary['AOS_Invoices']['fields']['sage_ref_shipping']['reportable']=true;

 

 // created: 2014-12-19 16:21:50
$dictionary['AOS_Invoices']['fields']['sage_sop_number_c']['labelValue']='Sage SOP Number';
$dictionary['AOS_Invoices']['fields']['sage_sop_number_c']['unified_search']='1';

 

 // created: 2015-04-14 16:52:33
$dictionary['AOS_Invoices']['fields']['salesordersystemnotes_c']['labelValue']='Sales Order System Notes';

 

 // created: 2014-05-07 13:50:15
$dictionary['AOS_Invoices']['fields']['sales_order_status_c']['labelValue']='sales order status';

 

 // created: 2014-06-12 14:51:11
$dictionary['AOS_Invoices']['fields']['service_c']['labelValue']='Service';

 

 // created: 2015-01-09 10:03:33
$dictionary['AOS_Invoices']['fields']['shipping_company_c']['labelValue']='Shipping Company';

 

 // created: 2015-01-09 10:04:05
$dictionary['AOS_Invoices']['fields']['shipping_contact_c']['labelValue']='Shipping Contact';

 

 // created: 2014-05-07 13:50:15
$dictionary['AOS_Invoices']['fields']['shipping_method_c']['labelValue']='Shipping Method';

 

 // created: 2014-05-07 13:50:15
$dictionary['AOS_Invoices']['fields']['shipping_service_terms_c']['labelValue']='Shipping Service/Terms';

 

 // created: 2014-05-07 13:50:15
$dictionary['AOS_Invoices']['fields']['so_notes_external_c']['labelValue']='Notes for External Documents';

 

 // created: 2014-05-07 13:50:15
$dictionary['AOS_Invoices']['fields']['so_notes_internal_c']['labelValue']='Notes for Internal Use';

 

 // created: 2014-05-05 15:49:37
$dictionary['AOS_Invoices']['fields']['status']['default']='Unpaid';
$dictionary['AOS_Invoices']['fields']['status']['merge_filter']='disabled';

 

 // created: 2014-05-05 15:47:56
$dictionary['AOS_Invoices']['fields']['template_ddown_c']['default']='^^';
$dictionary['AOS_Invoices']['fields']['template_ddown_c']['merge_filter']='disabled';
$dictionary['AOS_Invoices']['fields']['template_ddown_c']['reportable']=true;

 

 // created: 2014-05-07 13:50:15
$dictionary['AOS_Invoices']['fields']['test_certificates_required_c']['labelValue']='Test Certificates Required';

 

 // created: 2014-06-12 14:45:15
$dictionary['AOS_Invoices']['fields']['trackingno_c']['labelValue']='Tracking No';

 

 // created: 2014-06-12 14:49:28
$dictionary['AOS_Invoices']['fields']['trackingurl_c']['labelValue']='Tracking Link';

 
?>