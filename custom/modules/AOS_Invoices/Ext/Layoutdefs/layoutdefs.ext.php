<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2015-01-12 15:28:24
$layout_defs["AOS_Invoices"]["subpanel_setup"]['aos_invoices_securitygroups_1'] = array (
  'order' => 100,
  'module' => 'SecurityGroups',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_AOS_INVOICES_SECURITYGROUPS_1_FROM_SECURITYGROUPS_TITLE',
  'get_subpanel_data' => 'aos_invoices_securitygroups_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2014-05-08 10:48:41
$layout_defs["AOS_Invoices"]["subpanel_setup"]['aos_products_aos_invoices_1'] = array (
  'order' => 100,
  'module' => 'AOS_Products',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_AOS_PRODUCTS_AOS_INVOICES_1_FROM_AOS_PRODUCTS_TITLE',
  'get_subpanel_data' => 'aos_products_aos_invoices_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2014-11-12 17:42:29
$layout_defs["AOS_Invoices"]["subpanel_setup"]['documents_aos_invoices_1'] = array (
  'order' => 100,
  'module' => 'Documents',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_DOCUMENTS_AOS_INVOICES_1_FROM_DOCUMENTS_TITLE',
  'get_subpanel_data' => 'documents_aos_invoices_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


/**
 * hide_subpanel.php
 * @author SalesAgility (Andrew Mclaughlan) <support@salesagility.com>
 * Date: 18/04/14
 * Comments
 */
unset($layout_defs['AOS_Invoices']['subpanel_setup']['aos_products_aos_invoices_1']);

//auto-generated file DO NOT EDIT
$layout_defs['AOS_Invoices']['subpanel_setup']['documents_aos_invoices_1']['override_subpanel_name'] = 'AOS_Invoices_subpanel_documents_aos_invoices_1';

?>