<?php
$viewdefs ['Opportunities'] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL1' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_PANEL_ASSIGNMENT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
          1 => 'account_name',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'currency_id',
            'comment' => 'Currency used for display purposes',
            'label' => 'LBL_CURRENCY',
          ),
          1 => 'date_closed',
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'amount',
            'label' => '{$MOD.LBL_AMOUNT} ({$CURRENCY})',
          ),
          1 => 
          array (
            'name' => 'assigned_user_name',
            'label' => 'LBL_ASSIGNED_TO',
          ),
        ),
        3 => 
        array (
          0 => 'sales_stage',
          1 => 'opportunity_type',
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'reason_won_lost_c',
            'studio' => 'visible',
            'label' => 'LBL_REASON_WON_LOST',
          ),
          1 => 'lead_source',
        ),
        5 => 
        array (
          0 => 'campaign_name',
          1 => 
          array (
            'name' => 'lead_source_description_c',
            'studio' => 'visible',
            'label' => 'LBL_LEAD_SOURCE_DESCRIPTION',
          ),
        ),
        6 => 
        array (
          0 => 'probability',
          1 => 
          array (
            'name' => 'competition_c',
            'studio' => 'visible',
            'label' => 'LBL_COMPETITION',
          ),
        ),
        7 => 
        array (
          0 => 'next_step',
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 'description',
            'nl2br' => true,
          ),
        ),
      ),
      'lbl_editview_panel1' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'include_in_forecast_c',
            'label' => 'LBL_INCLUDE_IN_FORECAST',
          ),
          1 => 
          array (
            'name' => 'expected_despatch_date_c',
            'label' => 'LBL_EXPECTED_DESPATCH_DATE',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'aos_product_categories_opportunities_1_name',
          ),
          1 => 
          array (
            'name' => 'aos_products_opportunities_1_name',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'forecast_quantity_c',
            'label' => 'LBL_FORECAST_QUANTITY',
          ),
          1 => 
          array (
            'name' => 'total_forecast_value_gbp_c',
            'label' => 'LBL_TOTAL_FORECAST_VALUE_GBP',
          ),
        ),
      ),
      'LBL_PANEL_ASSIGNMENT' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'created_by_name',
            'label' => 'LBL_CREATED',
          ),
          1 => 
          array (
            'name' => 'date_entered',
            'customCode' => '{$fields.date_entered.value} {$APP.LBL_BY} {$fields.created_by_name.value}',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'modified_by_name',
            'label' => 'LBL_MODIFIED_NAME',
          ),
          1 => 
          array (
            'name' => 'date_modified',
            'label' => 'LBL_DATE_MODIFIED',
            'customCode' => '{$fields.date_modified.value} {$APP.LBL_BY} {$fields.modified_by_name.value}',
          ),
        ),
      ),
    ),
  ),
);
?>
