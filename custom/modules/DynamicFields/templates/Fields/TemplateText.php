<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

require_once('modules/DynamicFields/templates/Fields/TemplateText.php');
class CustomTemplateText extends TemplateText{

    /**
     * save
     *
     * This function says the field template by calling the DynamicField addFieldObject function.  It then
     * checks to see if updates are needed for the SearchFields.php file.  In the event that the unified_search
     * member variable is set to true, a search field definition is updated/created to the SearchFields.php file.
     *
     * @param DynamicField $df
     */
    function save($df){
        //	    $GLOBALS['log']->debug('saving field: '.print_r($this,true));
        $df->addFieldObject($this);

        require_once('modules/ModuleBuilder/parsers/parser.searchfields.php');
        $searchFieldParser = new ParserSearchFields( $df->getModuleName() , $df->getPackageName() ) ;
        //If unified_search is enabled for this field, then create the SearchFields entry
        $fieldName = $this->get_field_name($df->getModuleName(), $this->name);
        if($this->unified_search && !isset($searchFieldParser->searchFields[$df->getModuleName()][$fieldName]))
        {
            $searchFieldParser->addSearchField($fieldName, array('query_type'=>'default'));
            $searchFieldParser->saveSearchFields($searchFieldParser->searchFields);
        }

        else if(!$this->unified_search && isset($searchFieldParser->searchFields[$df->getModuleName()][$fieldName])){
            $searchFieldParser->removeSearchField($fieldName);
            $searchFieldParser->saveSearchFields($searchFieldParser->searchFields);
        }
    }



}


?>
