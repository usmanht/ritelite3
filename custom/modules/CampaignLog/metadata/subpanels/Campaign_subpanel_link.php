<?php
// created: 2015-07-14 17:45:21
$subpanel_layout['list_fields'] = array (
  'recipient_name' => 
  array (
    'vname' => 'LBL_LIST_RECIPIENT_NAME',
    'width' => '14%',
    'sortable' => false,
    'default' => true,
  ),
  'recipient_email' => 
  array (
    'vname' => 'LBL_LIST_RECIPIENT_EMAIL',
    'width' => '14%',
    'sortable' => false,
    'default' => true,
  ),
  'marketing_name' => 
  array (
    'vname' => 'LBL_LIST_MARKETING_NAME',
    'width' => '14%',
    'sortable' => false,
    'default' => true,
  ),
  'activity_type' => 
  array (
    'vname' => 'LBL_ACTIVITY_TYPE',
    'width' => '14%',
    'default' => true,
  ),
  'activity_date' => 
  array (
    'vname' => 'LBL_ACTIVITY_DATE',
    'width' => '14%',
    'default' => true,
  ),
  'target_tracker_key' => 
  array (
    'type' => 'varchar',
    'vname' => 'LBL_TARGET_TRACKER_KEY',
    'width' => '10%',
    'default' => true,
  ),
  'related_name' => 
  array (
    'widget_class' => 'SubPanelDetailViewLink',
    'target_record_key' => 'related_id',
    'target_module_key' => 'related_type',
    'parent_id' => 'target_id',
    'parent_module' => 'target_type',
    'vname' => 'LBL_RELATED',
    'width' => '20%',
    'sortable' => false,
    'default' => true,
  ),
  'hits' => 
  array (
    'vname' => 'LBL_HITS',
    'width' => '5%',
    'default' => true,
  ),
  'target_id' => 
  array (
    'usage' => 'query_only',
  ),
  'target_type' => 
  array (
    'usage' => 'query_only',
  ),
  'related_id' => 
  array (
    'usage' => 'query_only',
  ),
  'related_type' => 
  array (
    'usage' => 'query_only',
  ),
);