<?php 
 //WARNING: The contents of this file are auto-generated


$layout_defs['ProjectTask']['subpanel_setup']['timesheet'] =  array(
  'order' => 2,
  'module' => 'Timesheet',
  'get_subpanel_data' => 'timesheet',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_TIMESHEET_SUBPANEL_TITLE',
); 



$layout_defs['ProjectTask']['subpanel_setup']['securitygroups'] = array(
	'top_buttons' => array(array('widget_class' => 'SubPanelTopSelectButton', 'popup_module' => 'SecurityGroups', 'mode' => 'MultiSelect'),),
	'order' => 900,
	'sort_by' => 'name',
	'sort_order' => 'asc',
	'module' => 'SecurityGroups',
	'refresh_page'=>1,
	'subpanel_name' => 'default',
	'get_subpanel_data' => 'SecurityGroups',
	'add_subpanel_data' => 'securitygroup_id',
	'title_key' => 'LBL_SECURITYGROUPS_SUBPANEL_TITLE',
);






//auto-generated file DO NOT EDIT
$layout_defs['ProjectTask']['subpanel_setup']['timesheet']['override_subpanel_name'] = 'ProjectTask_subpanel_timesheet';

?>