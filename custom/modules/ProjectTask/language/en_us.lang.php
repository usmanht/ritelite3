<?php
// created: 2016-04-25 10:44:07
$mod_strings = array (
  'LBL_SECURITYGROUPS_SUBPANEL_TITLE' => 'Security Groups',
  'LBL_BUDGET' => 'Budget',
  'LBL_ACTUAL' => 'Total Actual Effort (hrs)',
  'LBL_BILLABLE' => 'Total Billable Effort (hrs)',
  'LBL_TIMESHEET_SUBPANEL_TITLE' => 'Timesheets',
  'LBL_EDITVIEW_PANEL1' => 'Relationship',
  'LBL_EDITVIEW_PANEL2' => 'Budget & Costs',
  'LBL_EDITVIEW_PANEL3' => 'New Panel 3',
);