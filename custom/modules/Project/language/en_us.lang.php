<?php
// created: 2016-04-25 10:54:31
$mod_strings = array (
  'LBL_SECURITYGROUPS_SUBPANEL_TITLE' => 'Security Groups',
  'LBL_BUDGET' => 'Budget',
  'LBL_ACTUAL' => 'Total Actual Effort (hrs)',
  'LBL_BILLABLE' => 'Total Billable Effort (hrs)',
  'LBL_TIMESHEET_SUBPANEL_TITLE' => 'Timesheets',
  'LBL_PROJECT_TASKS_SUBPANEL_TITLE' => 'Project Tasks',
  'LBL_EDITVIEW_PANEL1' => 'Budget & Costs',
);