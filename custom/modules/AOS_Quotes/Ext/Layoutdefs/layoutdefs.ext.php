<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2014-08-26 09:34:04
$layout_defs["AOS_Quotes"]["subpanel_setup"]['aos_quotes_calls_1'] = array (
  'order' => 100,
  'module' => 'Calls',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_AOS_QUOTES_CALLS_1_FROM_CALLS_TITLE',
  'get_subpanel_data' => 'aos_quotes_calls_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2014-05-13 17:59:33
$layout_defs["AOS_Quotes"]["subpanel_setup"]['documents_aos_quotes_1'] = array (
  'order' => 100,
  'module' => 'Documents',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_DOCUMENTS_AOS_QUOTES_1_FROM_DOCUMENTS_TITLE',
  'get_subpanel_data' => 'documents_aos_quotes_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2014-05-02 12:12:37
$layout_defs["AOS_Quotes"]["subpanel_setup"]['securitygroups_aos_quotes_1'] = array (
  'order' => 100,
  'module' => 'SecurityGroups',
  'subpanel_name' => 'admin',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SECURITYGROUPS_AOS_QUOTES_1_FROM_SECURITYGROUPS_TITLE',
  'get_subpanel_data' => 'securitygroups_aos_quotes_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>