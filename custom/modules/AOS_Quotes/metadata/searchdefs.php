<?php
$module_name = 'AOS_Quotes';
$_module_name = 'aos_quotes';
$searchdefs [$module_name] = 
array (
  'layout' => 
  array (
    'basic_search' => 
    array (
      0 => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      1 => 
      array (
        'name' => 'current_user_only',
        'label' => 'LBL_CURRENT_USER_FILTER',
        'type' => 'bool',
        'default' => true,
        'width' => '10%',
      ),
      2 => 
      array (
        'type' => 'int',
        'label' => 'LBL_QUOTE_NUMBER',
        'default' => true,
        'width' => '10%',
        'name' => 'number',
      ),
      3 => 
      array (
        'type' => 'enum',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_STAGE',
        'width' => '10%',
        'name' => 'stage',
      ),
      4 => 
      array (
        'type' => 'varchar',
        'default' => true,
        'label' => 'LBL_ACCESSQUOTEREF',
        'width' => '10%',
        'name' => 'accessquoteref_c',
      ),
    ),
    'advanced_search' => 
    array (
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'billing_account' => 
      array (
        'name' => 'billing_account',
        'default' => true,
        'width' => '10%',
      ),
      'billing_contact' => 
      array (
        'name' => 'billing_contact',
        'default' => true,
        'width' => '10%',
      ),
      'accounts_aos_quotes_1_name' => 
      array (
        'type' => 'relate',
        'link' => true,
        'label' => 'LBL_ACCOUNTS_AOS_QUOTES_1_FROM_ACCOUNTS_TITLE',
        'id' => 'ACCOUNTS_AOS_QUOTES_1ACCOUNTS_IDA',
        'width' => '10%',
        'default' => true,
        'name' => 'accounts_aos_quotes_1_name',
      ),
      'contacts_aos_quotes_1_name' => 
      array (
        'type' => 'relate',
        'link' => true,
        'label' => 'LBL_CONTACTS_AOS_QUOTES_1_FROM_CONTACTS_TITLE',
        'id' => 'CONTACTS_AOS_QUOTES_1CONTACTS_IDA',
        'width' => '10%',
        'default' => true,
        'name' => 'contacts_aos_quotes_1_name',
      ),
      'number' => 
      array (
        'name' => 'number',
        'default' => true,
        'width' => '10%',
      ),
      'opportunity' => 
      array (
        'type' => 'relate',
        'studio' => 'visible',
        'label' => 'LBL_OPPORTUNITY',
        'id' => 'OPPORTUNITY_ID',
        'link' => true,
        'width' => '10%',
        'default' => true,
        'name' => 'opportunity',
      ),
      'total_amount' => 
      array (
        'name' => 'total_amount',
        'default' => true,
        'width' => '10%',
      ),
      'expiration' => 
      array (
        'name' => 'expiration',
        'default' => true,
        'width' => '10%',
      ),
      'stage' => 
      array (
        'name' => 'stage',
        'default' => true,
        'width' => '10%',
      ),
      'assigned_user_id' => 
      array (
        'name' => 'assigned_user_id',
        'type' => 'enum',
        'label' => 'LBL_ASSIGNED_TO',
        'function' => 
        array (
          'name' => 'get_user_array',
          'params' => 
          array (
            0 => false,
          ),
        ),
        'default' => true,
        'width' => '10%',
      ),
      'accessquoteref_c' => 
      array (
        'type' => 'varchar',
        'default' => true,
        'label' => 'LBL_ACCESSQUOTEREF',
        'width' => '10%',
        'name' => 'accessquoteref_c',
      ),
      'cases_aos_quotes_1_name' => 
      array (
        'type' => 'relate',
        'link' => true,
        'label' => 'LBL_CASES_AOS_QUOTES_1_FROM_CASES_TITLE',
        'id' => 'CASES_AOS_QUOTES_1CASES_IDA',
        'width' => '10%',
        'default' => true,
        'name' => 'cases_aos_quotes_1_name',
      ),
      'searchlabel_c' => 
      array (
        'type' => 'varchar',
        'default' => true,
        'label' => 'LBL_SEARCHLABEL',
        'width' => '10%',
        'name' => 'searchlabel_c',
      ),
      'date_entered' => 
      array (
        'type' => 'datetime',
        'label' => 'LBL_DATE_ENTERED',
        'width' => '10%',
        'default' => true,
        'name' => 'date_entered',
      ),
      'current_user_only' => 
      array (
        'label' => 'LBL_CURRENT_USER_FILTER',
        'type' => 'bool',
        'default' => true,
        'width' => '10%',
        'name' => 'current_user_only',
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
    'maxColumnsBasic' => '3',
  ),
);
?>
