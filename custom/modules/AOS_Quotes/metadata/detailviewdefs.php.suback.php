<?php
// created: 2016-03-04 15:10:16
$viewdefs['AOS_Quotes']['DetailView'] = array (
  'templateMeta' => 
  array (
    'form' => 
    array (
      'buttons' => 
      array (
        0 => 'EDIT',
        1 => 'DUPLICATE',
        2 => 'DELETE',
        3 => 'FIND_DUPLICATES',
        4 => 
        array (
          'customCode' => '<input type="button" class="button" onClick="showPopup(\'pdf\');" value="{$MOD.LBL_PRINT_AS_PDF}">',
        ),
        5 => 
        array (
          'customCode' => '<input type="button" class="button" onClick="showPopup(\'emailpdf\');" value="{$MOD.LBL_EMAIL_PDF}">',
        ),
        6 => 
        array (
          'customCode' => '<input type="button" class="button" onClick="showPopup(\'email\');" value="{$MOD.LBL_EMAIL_QUOTE}">',
        ),
        7 => 
        array (
          'customCode' => '<input type="submit" class="button" onClick="this.form.action.value=\'createContract\';" value="{$MOD.LBL_CREATE_CONTRACT}">',
          'sugar_html' => 
          array (
            'type' => 'submit',
            'value' => '{$MOD.LBL_CREATE_CONTRACT}',
            'htmlOptions' => 
            array (
              'class' => 'button',
              'id' => 'create_contract_button',
              'title' => '{$MOD.LBL_CREATE_CONTRACT}',
              'onclick' => 'this.form.action.value=\'createContract\';',
              'name' => 'Create Contract',
            ),
          ),
        ),
        8 => 
        array (
          'customCode' => '<input type="submit" class="button" onClick="this.form.action.value=\'converToInvoice\';" value="{$MOD.LBL_CONVERT_TO_INVOICE}">',
          'sugar_html' => 
          array (
            'type' => 'submit',
            'value' => '{$MOD.LBL_CONVERT_TO_INVOICE}',
            'htmlOptions' => 
            array (
              'class' => 'button',
              'id' => 'convert_to_invoice_button',
              'title' => '{$MOD.LBL_CONVERT_TO_INVOICE}',
              'onclick' => 'this.form.action.value=\'converToInvoice\';',
              'name' => 'Convert to Invoice',
            ),
          ),
        ),
      ),
    ),
    'maxColumns' => '2',
    'widths' => 
    array (
      0 => 
      array (
        'label' => '10',
        'field' => '30',
      ),
      1 => 
      array (
        'label' => '10',
        'field' => '30',
      ),
    ),
    'useTabs' => false,
    'tabDefs' => 
    array (
      'LBL_ACCOUNT_INFORMATION' => 
      array (
        'newTab' => false,
        'panelDefault' => 'expanded',
      ),
      'LBL_ADDRESS_INFORMATION' => 
      array (
        'newTab' => false,
        'panelDefault' => 'expanded',
      ),
      'LBL_LINE_ITEMS' => 
      array (
        'newTab' => false,
        'panelDefault' => 'expanded',
      ),
      'LBL_EDITVIEW_PANEL1' => 
      array (
        'newTab' => false,
        'panelDefault' => 'expanded',
      ),
      'LBL_EDITVIEW_PANEL2' => 
      array (
        'newTab' => false,
        'panelDefault' => 'expanded',
      ),
    ),
    'syncDetailEditViews' => true,
  ),
  'panels' => 
  array (
    'lbl_account_information' => 
    array (
      0 => 
      array (
        0 => 
        array (
          'name' => 'name',
          'label' => 'LBL_NAME',
        ),
        1 => 
        array (
          'name' => 'opportunity',
          'label' => 'LBL_OPPORTUNITY',
        ),
      ),
      1 => 
      array (
        0 => 
        array (
          'name' => 'number',
          'label' => 'LBL_QUOTE_NUMBER',
        ),
        1 => 
        array (
          'name' => 'stage',
          'label' => 'LBL_STAGE',
        ),
      ),
      2 => 
      array (
        0 => 
        array (
          'name' => 'expiration',
          'label' => 'LBL_EXPIRATION',
        ),
        1 => '',
      ),
      3 => 
      array (
        0 => 
        array (
          'name' => 'assigned_user_name',
          'label' => 'LBL_ASSIGNED_TO',
        ),
        1 => 
        array (
          'name' => 'term',
          'label' => 'LBL_TERM',
        ),
      ),
      4 => 
      array (
        0 => 
        array (
          'name' => 'approval_status',
          'label' => 'LBL_APPROVAL_STATUS',
        ),
        1 => 
        array (
          'name' => 'approval_issue',
          'label' => 'LBL_APPROVAL_ISSUE',
        ),
      ),
      5 => 
      array (
        0 => 
        array (
          'name' => 'accessquoteid_c',
          'label' => 'LBL_ACCESSQUOTEID',
        ),
        1 => 
        array (
          'name' => 'accessquoteref_c',
          'label' => 'LBL_ACCESSQUOTEREF',
        ),
      ),
      6 => 
      array (
        0 => 
        array (
          'name' => 'searchlabel_c',
          'label' => 'LBL_SEARCHLABEL',
        ),
        1 => 
        array (
          'name' => 'notes_currency_exchange_rate_c',
          'studio' => 'visible',
          'label' => 'LBL_NOTES_CURRENCY_EXCHANGE_RATE',
        ),
      ),
    ),
    'lbl_address_information' => 
    array (
      0 => 
      array (
        0 => 
        array (
          'name' => 'sage_ref_billing',
          'studio' => 'visible',
          'label' => 'LBL_SAGE_REF',
        ),
        1 => 
        array (
          'name' => 'sage_ref_shipping',
          'studio' => 'visible',
          'label' => 'LBL_SAGE_REF1',
        ),
      ),
      1 => 
      array (
        0 => 
        array (
          'name' => 'billing_account',
          'label' => 'LBL_BILLING_ACCOUNT',
        ),
        1 => 
        array (
          'name' => 'accounts_aos_quotes_1_name',
        ),
      ),
      2 => 
      array (
        0 => 
        array (
          'name' => 'billing_contact',
          'label' => 'LBL_BILLING_CONTACT',
        ),
        1 => 
        array (
          'name' => 'contacts_aos_quotes_1_name',
        ),
      ),
      3 => 
      array (
        0 => '',
        1 => 
        array (
          'name' => 'shipping_company_c',
          'label' => 'LBL_SHIPPING_COMPANY',
        ),
      ),
      4 => 
      array (
        0 => '',
        1 => 
        array (
          'name' => 'shipping_contact_c',
          'label' => 'LBL_SHIPPING_CONTACT',
        ),
      ),
      5 => 
      array (
        0 => 
        array (
          'name' => 'billing_address_street',
          'label' => 'LBL_BILLING_ADDRESS',
          'type' => 'address',
          'displayParams' => 
          array (
            'key' => 'billing',
          ),
        ),
        1 => 
        array (
          'name' => 'shipping_address_street',
          'label' => 'LBL_SHIPPING_ADDRESS',
          'type' => 'address',
          'displayParams' => 
          array (
            'key' => 'shipping',
          ),
        ),
      ),
    ),
    'lbl_line_items' => 
    array (
      0 => 
      array (
        0 => 
        array (
          'name' => 'currency_id',
          'studio' => 'visible',
          'label' => 'LBL_CURRENCY',
        ),
      ),
      1 => 
      array (
        0 => 
        array (
          'name' => 'line_items',
          'label' => 'LBL_LINE_ITEMS',
        ),
      ),
      2 => 
      array (
        0 => 
        array (
          'name' => 'total_amt',
          'label' => 'LBL_TOTAL_AMT',
        ),
      ),
      3 => 
      array (
        0 => 
        array (
          'name' => 'discount_amount',
          'label' => 'LBL_DISCOUNT_AMOUNT',
        ),
      ),
      4 => 
      array (
        0 => 
        array (
          'name' => 'subtotal_amount',
          'label' => 'LBL_SUBTOTAL_AMOUNT',
        ),
      ),
      5 => 
      array (
        0 => 
        array (
          'name' => 'shipping_amount',
          'label' => 'LBL_SHIPPING_AMOUNT',
        ),
      ),
      6 => 
      array (
        0 => 
        array (
          'name' => 'shipping_tax_amt',
          'label' => 'LBL_SHIPPING_TAX_AMT',
        ),
      ),
      7 => 
      array (
        0 => 
        array (
          'name' => 'tax_amount',
          'label' => 'LBL_TAX_AMOUNT',
        ),
      ),
      8 => 
      array (
        0 => 
        array (
          'name' => 'total_amount',
          'label' => 'LBL_GRAND_TOTAL',
        ),
      ),
    ),
    'lbl_editview_panel1' => 
    array (
      0 => 
      array (
        0 => 
        array (
          'name' => 'additiona_text_for_quote_c',
          'studio' => 'visible',
          'label' => 'LBL_ADDITIONA_TEXT_FOR_QUOTE',
        ),
      ),
    ),
    'lbl_editview_panel2' => 
    array (
      0 => 
      array (
        0 => 
        array (
          'name' => 'created_by_name',
          'label' => 'LBL_CREATED',
        ),
        1 => 
        array (
          'name' => 'date_entered',
          'comment' => 'Date record created',
          'label' => 'LBL_DATE_ENTERED',
        ),
      ),
      1 => 
      array (
        0 => 
        array (
          'name' => 'modified_by_name',
          'label' => 'LBL_MODIFIED_NAME',
        ),
        1 => 
        array (
          'name' => 'date_modified',
          'comment' => 'Date record last modified',
          'label' => 'LBL_DATE_MODIFIED',
        ),
      ),
    ),
  ),
);