<?php
// created: 2018-02-23 10:48:58
$mod_strings = array (
  'LBL_NAME' => 'Stock Code',
  'LBL_PRICE' => 'List Price',
  'LBL_COST' => 'Cost Price',
  'LBL_LOWEST_SALE_PRICE' => 'Lowest Sale Price',
  'LBL_QUANTITY' => 'Quantity in Stock',
  'LBL_BACK_ORDER_QUANTITY' => 'Quantity on Back Order ',
  'LBL_NINETY_DAYS_SALES' => '90 Days Sales Figure',
  'LBL_CLASSIFICATION' => 'Classification',
  'LBL_COMMODITY_CODE' => 'Commodity Code',
  'LBL_PART_NUMBER' => 'Part Number',
  'LBL_AOS_PRODUCTS_BUGS_1_FROM_BUGS_TITLE' => 'Bugs (Main Stock Code)',
  'LBL_BUGS_AOS_PRODUCTS_1_FROM_BUGS_TITLE' => 'Bugs (Associated Stock Code)',
  'LBL_CUSTOMERS_PURCHASED_PRODUCTS_SUBPANEL_TITLE' => 'Quotes',
  'LBL_AOS_PRODUCTS_AOS_INVOICES_1_FROM_AOS_INVOICES_TITLE' => 'Sales Orders',
  'LBL_CATEGORY' => 'Category',
);