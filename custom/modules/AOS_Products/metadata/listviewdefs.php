<?php
// created: 2016-03-04 15:10:16
$listViewDefs['AOS_Products'] = array (
  'NAME' => 
  array (
    'width' => '15%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'PART_NUMBER' => 
  array (
    'width' => '10%',
    'label' => 'LBL_PART_NUMBER',
    'default' => true,
  ),
  'DESCRIPTION' => 
  array (
    'type' => 'text',
    'label' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => '15%',
    'default' => true,
  ),
  'AOS_PRODUCT_CATEGORY_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_AOS_PRODUCT_CATEGORYS_NAME',
    'id' => 'AOS_PRODUCT_CATEGORY_ID',
    'width' => '10%',
    'default' => true,
    'studio' => 'visible',
    'related_fields' => 
    array (
      0 => 'aos_product_category_id',
    ),
  ),
  'COST' => 
  array (
    'width' => '5%',
    'label' => 'LBL_COST',
    'currency_format' => true,
    'default' => true,
  ),
  'PRICE' => 
  array (
    'width' => '5%',
    'label' => 'LBL_PRICE',
    'currency_format' => true,
    'default' => true,
  ),
  'LOWEST_SALE_PRICE_C' => 
  array (
    'type' => 'currency',
    'default' => true,
    'label' => 'LBL_LOWEST_SALE_PRICE',
    'currency_format' => true,
    'width' => '5%',
  ),
  'CLASSIFICATION_C' => 
  array (
    'type' => 'int',
    'default' => true,
    'label' => 'LBL_CLASSIFICATION',
    'width' => '10%',
  ),
  'QUANTITY_C' => 
  array (
    'type' => 'int',
    'default' => true,
    'label' => 'LBL_QUANTITY',
    'width' => '5%',
  ),
  'BACK_ORDER_QUANTITY_C' => 
  array (
    'type' => 'int',
    'default' => true,
    'label' => 'LBL_BACK_ORDER_QUANTITY',
    'width' => '5%',
  ),
  'NINETY_DAYS_SALES_C' => 
  array (
    'type' => 'currency',
    'default' => true,
    'label' => 'LBL_NINETY_DAYS_SALES',
    'currency_format' => true,
    'width' => '5%',
  ),
  'COMMODITY_CODE_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_COMMODITY_CODE',
    'width' => '10%',
  ),
  'CREATED_BY_NAME' => 
  array (
    'width' => '10%',
    'label' => 'LBL_CREATED',
    'default' => true,
    'module' => 'Users',
    'link' => true,
    'id' => 'CREATED_BY',
  ),
  'DATE_ENTERED' => 
  array (
    'width' => '5%',
    'label' => 'LBL_DATE_ENTERED',
    'default' => true,
  ),
);