<?php
$popupMeta = array (
    'moduleMain' => 'AOS_Products',
    'varName' => 'AOS_Products',
    'orderBy' => 'aos_products.name',
    'whereClauses' => array (
  'name' => 'aos_products.name',
  'part_number' => 'aos_products.part_number',
  'cost' => 'aos_products.cost',
  'price' => 'aos_products.price',
  'created_by' => 'aos_products.created_by',
  'description' => 'aos_products.description',
),
    'searchInputs' => array (
  1 => 'name',
  4 => 'part_number',
  5 => 'cost',
  6 => 'price',
  7 => 'created_by',
  8 => 'description',
),
    'searchdefs' => array (
  'name' => 
  array (
    'name' => 'name',
    'width' => '10%',
  ),
  'part_number' => 
  array (
    'name' => 'part_number',
    'width' => '10%',
  ),
  'cost' => 
  array (
    'name' => 'cost',
    'width' => '10%',
  ),
  'price' => 
  array (
    'name' => 'price',
    'width' => '10%',
  ),
  'created_by' => 
  array (
    'name' => 'created_by',
    'label' => 'LBL_CREATED',
    'type' => 'enum',
    'function' => 
    array (
      'name' => 'get_user_array',
      'params' => 
      array (
        0 => false,
      ),
    ),
    'width' => '10%',
  ),
  'description' => 
  array (
    'type' => 'text',
    'label' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => '10%',
    'name' => 'description',
  ),
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => '10%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'PART_NUMBER' => 
  array (
    'width' => '10%',
    'label' => 'LBL_PART_NUMBER',
    'default' => true,
    'name' => 'part_number',
  ),
  'DESCRIPTION' => 
  array (
    'type' => 'text',
    'label' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => '15%',
    'default' => true,
  ),
  'AOS_PRODUCT_CATEGORY_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_AOS_PRODUCT_CATEGORYS_NAME',
    'id' => 'AOS_PRODUCT_CATEGORY_ID',
    'width' => '10%',
    'default' => true,
  ),
  'COST' => 
  array (
    'width' => '5%',
    'label' => 'LBL_COST',
    'default' => true,
    'name' => 'cost',
  ),
  'PRICE' => 
  array (
    'width' => '5%',
    'label' => 'LBL_PRICE',
    'default' => true,
    'name' => 'price',
  ),
  'LOWEST_SALE_PRICE_C' => 
  array (
    'type' => 'currency',
    'default' => true,
    'label' => 'LBL_LOWEST_SALE_PRICE',
    'currency_format' => true,
    'width' => '5%',
  ),
  'CLASSIFICATION_C' => 
  array (
    'type' => 'int',
    'default' => true,
    'label' => 'LBL_CLASSIFICATION',
    'width' => '10%',
  ),
  'QUANTITY_C' => 
  array (
    'type' => 'int',
    'default' => true,
    'label' => 'LBL_QUANTITY',
    'width' => '5%',
  ),
  'BACK_ORDER_QUANTITY_C' => 
  array (
    'type' => 'int',
    'default' => true,
    'label' => 'LBL_BACK_ORDER_QUANTITY',
    'width' => '5%',
  ),
  'NINETY_DAYS_SALES_C' => 
  array (
    'type' => 'currency',
    'default' => true,
    'label' => 'LBL_NINETY_DAYS_SALES',
    'currency_format' => true,
    'width' => '5%',
  ),
  'COMMODITY_CODE_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_COMMODITY_CODE',
    'width' => '10%',
  ),
  'CREATED_BY_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_CREATED',
    'id' => 'CREATED_BY',
    'width' => '10%',
    'default' => true,
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_ENTERED',
    'width' => '5%',
    'default' => true,
  ),
),
);
