<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2017-10-06 11:09:43
$dictionary["Case"]["fields"]["aos_products_cases_1"] = array (
  'name' => 'aos_products_cases_1',
  'type' => 'link',
  'relationship' => 'aos_products_cases_1',
  'source' => 'non-db',
  'module' => 'AOS_Products',
  'bean_name' => 'AOS_Products',
  'vname' => 'LBL_AOS_PRODUCTS_CASES_1_FROM_AOS_PRODUCTS_TITLE',
  'id_name' => 'aos_products_cases_1aos_products_ida',
);
$dictionary["Case"]["fields"]["aos_products_cases_1_name"] = array (
  'name' => 'aos_products_cases_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_AOS_PRODUCTS_CASES_1_FROM_AOS_PRODUCTS_TITLE',
  'save' => true,
  'id_name' => 'aos_products_cases_1aos_products_ida',
  'link' => 'aos_products_cases_1',
  'table' => 'aos_products',
  'module' => 'AOS_Products',
  'rname' => 'name',
);
$dictionary["Case"]["fields"]["aos_products_cases_1aos_products_ida"] = array (
  'name' => 'aos_products_cases_1aos_products_ida',
  'type' => 'link',
  'relationship' => 'aos_products_cases_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_AOS_PRODUCTS_CASES_1_FROM_CASES_TITLE',
);


// created: 2017-10-05 08:41:36
$dictionary["Case"]["fields"]["cases_aos_quotes_1"] = array (
  'name' => 'cases_aos_quotes_1',
  'type' => 'link',
  'relationship' => 'cases_aos_quotes_1',
  'source' => 'non-db',
  'module' => 'AOS_Quotes',
  'bean_name' => 'AOS_Quotes',
  'side' => 'right',
  'vname' => 'LBL_CASES_AOS_QUOTES_1_FROM_AOS_QUOTES_TITLE',
);


// created: 2017-10-05 08:42:03
$dictionary["Case"]["fields"]["cases_aos_quotes_2"] = array (
  'name' => 'cases_aos_quotes_2',
  'type' => 'link',
  'relationship' => 'cases_aos_quotes_2',
  'source' => 'non-db',
  'module' => 'AOS_Quotes',
  'bean_name' => 'AOS_Quotes',
  'side' => 'right',
  'vname' => 'LBL_CASES_AOS_QUOTES_2_FROM_AOS_QUOTES_TITLE',
);


$dictionary['Case']['fields']['timesheet'] = array(
  'name' => 'timesheet',
  'type' => 'link',
  'relationship' => 'cases_timesheet',
  'source'=>'non-db'
);



$dictionary['Case']['fields']['SecurityGroups'] = array (
  	'name' => 'SecurityGroups',
    'type' => 'link',
	'relationship' => 'securitygroups_cases',
	'module'=>'SecurityGroups',
	'bean_name'=>'SecurityGroup',
    'source'=>'non-db',
	'vname'=>'LBL_SECURITYGROUPS',
);






 // created: 2015-02-03 09:39:35

 

 // created: 2017-11-16 09:29:22
$dictionary['Case']['fields']['arrival_method_c']['inline_edit']='1';
$dictionary['Case']['fields']['arrival_method_c']['labelValue']='Arrival Method';

 

 // created: 2015-02-03 09:39:35

 

 // created: 2015-02-03 09:39:35

 

 // created: 2018-05-16 14:49:08
$dictionary['Case']['fields']['freight_company_c']['inline_edit']='1';
$dictionary['Case']['fields']['freight_company_c']['labelValue']='Freight Company';

 

 // created: 2016-03-04 15:12:39

 

 // created: 2016-03-04 15:12:37

 

 // created: 2016-03-04 15:12:35

 

 // created: 2016-03-04 15:12:32

 

 // created: 2018-05-16 13:57:09
$dictionary['Case']['fields']['parcel_pallet_quantity_c']['inline_edit']='1';
$dictionary['Case']['fields']['parcel_pallet_quantity_c']['labelValue']='Parcel / Pallet Quantity';

 

 // created: 2018-05-16 14:39:45
$dictionary['Case']['fields']['return_freight_type_c']['inline_edit']='1';
$dictionary['Case']['fields']['return_freight_type_c']['labelValue']='Return Freight Type';

 
?>