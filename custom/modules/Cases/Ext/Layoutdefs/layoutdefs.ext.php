<?php 
 //WARNING: The contents of this file are auto-generated


$layout_defs['Cases']['subpanel_setup']['timesheet'] =  array(
  'order' => 2,
  'module' => 'Timesheet',
  'get_subpanel_data' => 'timesheet',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_TIMESHEET_SUBPANEL_TITLE',
); 


 // created: 2017-10-05 08:41:36
$layout_defs["Cases"]["subpanel_setup"]['cases_aos_quotes_1'] = array (
  'order' => 100,
  'module' => 'AOS_Quotes',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CASES_AOS_QUOTES_1_FROM_AOS_QUOTES_TITLE',
  'get_subpanel_data' => 'cases_aos_quotes_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2017-10-05 08:42:03
$layout_defs["Cases"]["subpanel_setup"]['cases_aos_quotes_2'] = array (
  'order' => 100,
  'module' => 'AOS_Quotes',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CASES_AOS_QUOTES_2_FROM_AOS_QUOTES_TITLE',
  'get_subpanel_data' => 'cases_aos_quotes_2',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);



$layout_defs['Cases']['subpanel_setup']['securitygroups'] = array(
	'top_buttons' => array(array('widget_class' => 'SubPanelTopSelectButton', 'popup_module' => 'SecurityGroups', 'mode' => 'MultiSelect'),),
	'order' => 900,
	'sort_by' => 'name',
	'sort_order' => 'asc',
	'module' => 'SecurityGroups',
	'refresh_page'=>1,
	'subpanel_name' => 'default',
	'get_subpanel_data' => 'SecurityGroups',
	'add_subpanel_data' => 'securitygroup_id',
	'title_key' => 'LBL_SECURITYGROUPS_SUBPANEL_TITLE',
);





//auto-generated file DO NOT EDIT
$layout_defs['Cases']['subpanel_setup']['contacts']['override_subpanel_name'] = 'Case_subpanel_contacts';

?>