<?php
// created: 2018-05-16 14:42:18
$mod_strings = array (
  'LBL_SECURITYGROUPS_SUBPANEL_TITLE' => 'Security Groups',
  'LBL_BUDGET' => 'Budget',
  'LBL_ACTUAL' => 'Total Actual Effort (hrs)',
  'LBL_BILLABLE' => 'Total Billable Effort (hrs)',
  'LBL_GENERATE_ACCESS_RETURN' => 'Generate Access Returns Number',
  'LBL_ACCESS_RETURNS_NUMBER' => 'Access Returns Number',
  'LBL_REQUIRED_CLOSURE_DATE' => 'Required Closure Date',
  'LBL_QUANTITY' => 'Quantity',
  'LBL_CREATE_WORKSHOP_REPAIR_QUOTE' => 'Create Workshop Quote',
  'LBL_RECEIVED_DATE_TIME' => 'Received Date/Time',
  'LBL_PRODUCT_CATEGORY' => 'Product Category',
  'LBL_ARRIVAL_METHOD' => 'Arrival Method',
  'LBL_AOS_PRODUCTS_CASES_1_FROM_AOS_PRODUCTS_TITLE' => 'Product Code',
  'LBL_CONTACTS_SUBPANEL_TITLE' => 'Contacts',
  'LBL_PARCEL_PALLET_QUANTITY' => 'Parcel / Pallet Quantity',
  'LBL_RETURN_FREIGHT_TYPE' => 'Return Freight Type',
  'LBL_FREIGHT_COMPANY' => 'Freight Company',
  'LBL_EDITVIEW_PANEL1' => 'Return Freight Details',
);