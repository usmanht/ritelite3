<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2018-01-11 10:37:23
$dictionary["Bug"]["fields"]["aos_product_categories_bugs_1"] = array (
  'name' => 'aos_product_categories_bugs_1',
  'type' => 'link',
  'relationship' => 'aos_product_categories_bugs_1',
  'source' => 'non-db',
  'module' => 'AOS_Product_Categories',
  'bean_name' => 'AOS_Product_Categories',
  'vname' => 'LBL_AOS_PRODUCT_CATEGORIES_BUGS_1_FROM_AOS_PRODUCT_CATEGORIES_TITLE',
  'id_name' => 'aos_product_categories_bugs_1aos_product_categories_ida',
);
$dictionary["Bug"]["fields"]["aos_product_categories_bugs_1_name"] = array (
  'name' => 'aos_product_categories_bugs_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_AOS_PRODUCT_CATEGORIES_BUGS_1_FROM_AOS_PRODUCT_CATEGORIES_TITLE',
  'save' => true,
  'id_name' => 'aos_product_categories_bugs_1aos_product_categories_ida',
  'link' => 'aos_product_categories_bugs_1',
  'table' => 'aos_product_categories',
  'module' => 'AOS_Product_Categories',
  'rname' => 'name',
);
$dictionary["Bug"]["fields"]["aos_product_categories_bugs_1aos_product_categories_ida"] = array (
  'name' => 'aos_product_categories_bugs_1aos_product_categories_ida',
  'type' => 'link',
  'relationship' => 'aos_product_categories_bugs_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_AOS_PRODUCT_CATEGORIES_BUGS_1_FROM_BUGS_TITLE',
);


// created: 2014-10-22 16:58:44
$dictionary["Bug"]["fields"]["bugs_aos_products_1"] = array (
  'name' => 'bugs_aos_products_1',
  'type' => 'link',
  'relationship' => 'bugs_aos_products_1',
  'source' => 'non-db',
  'module' => 'AOS_Products',
  'bean_name' => 'AOS_Products',
  'vname' => 'LBL_BUGS_AOS_PRODUCTS_1_FROM_AOS_PRODUCTS_TITLE',
);



$dictionary['Bug']['fields']['SecurityGroups'] = array (
  	'name' => 'SecurityGroups',
    'type' => 'link',
	'relationship' => 'securitygroups_bugs',
	'module'=>'SecurityGroups',
	'bean_name'=>'SecurityGroup',
    'source'=>'non-db',
	'vname'=>'LBL_SECURITYGROUPS',
);






 // created: 2018-01-11 10:46:35
$dictionary['Bug']['fields']['bug_required_close_date_c']['inline_edit']='1';
$dictionary['Bug']['fields']['bug_required_close_date_c']['options']='date_range_search_dom';
$dictionary['Bug']['fields']['bug_required_close_date_c']['labelValue']='Required close date';
$dictionary['Bug']['fields']['bug_required_close_date_c']['enable_range_search']='1';

 

 // created: 2014-10-22 17:01:32
$dictionary['Bug']['fields']['description']['required']=true;
$dictionary['Bug']['fields']['description']['audited']=true;
$dictionary['Bug']['fields']['description']['comments']='Full text of the note';
$dictionary['Bug']['fields']['description']['merge_filter']='disabled';

 

 // created: 2014-10-22 17:02:23
$dictionary['Bug']['fields']['priority']['default']='Urgent';
$dictionary['Bug']['fields']['priority']['required']=true;
$dictionary['Bug']['fields']['priority']['comments']='An indication of the priorty of the issue';
$dictionary['Bug']['fields']['priority']['merge_filter']='disabled';

 

 // created: 2014-10-22 17:03:00
$dictionary['Bug']['fields']['product_category']['len']=100;
$dictionary['Bug']['fields']['product_category']['required']=true;
$dictionary['Bug']['fields']['product_category']['audited']=true;
$dictionary['Bug']['fields']['product_category']['massupdate']='1';
$dictionary['Bug']['fields']['product_category']['comments']='Where the bug was discovered (ex: Accounts, Contacts, Leads)';
$dictionary['Bug']['fields']['product_category']['merge_filter']='disabled';

 

 // created: 2014-10-29 17:38:33
$dictionary['Bug']['fields']['resolution_description_c']['labelValue']='Resolution Description';

 

 // created: 2014-10-22 17:02:43
$dictionary['Bug']['fields']['source']['len']=100;
$dictionary['Bug']['fields']['source']['required']=true;
$dictionary['Bug']['fields']['source']['audited']=true;
$dictionary['Bug']['fields']['source']['massupdate']='1';
$dictionary['Bug']['fields']['source']['comments']='An indicator of how the bug was entered (ex: via web, email, etc.)';
$dictionary['Bug']['fields']['source']['merge_filter']='disabled';

 

 // created: 2014-10-22 17:02:11
$dictionary['Bug']['fields']['status']['default']='New';
$dictionary['Bug']['fields']['status']['required']=true;
$dictionary['Bug']['fields']['status']['massupdate']='1';
$dictionary['Bug']['fields']['status']['comments']='The status of the issue';
$dictionary['Bug']['fields']['status']['merge_filter']='disabled';

 

 // created: 2014-10-22 17:01:59
$dictionary['Bug']['fields']['type']['default']='Defect';
$dictionary['Bug']['fields']['type']['len']=100;
$dictionary['Bug']['fields']['type']['required']=true;
$dictionary['Bug']['fields']['type']['audited']=true;
$dictionary['Bug']['fields']['type']['massupdate']='1';
$dictionary['Bug']['fields']['type']['comments']='The type of issue (ex: issue, feature)';
$dictionary['Bug']['fields']['type']['merge_filter']='disabled';

 
?>