<?php
// created: 2018-01-11 10:57:02
$mod_strings = array (
  'LBL_SECURITYGROUPS_SUBPANEL_TITLE' => 'Security Groups',
  'LBL_DESCRIPTION' => 'Description:',
  'LBL_TYPE' => 'Type:',
  'LBL_STATUS' => 'Status:',
  'LBL_PRIORITY' => 'Priority:',
  'LBL_SOURCE' => 'Source:',
  'LBL_PRODUCT_CATEGORY' => 'Category:',
  'LBL_RESOLUTION_DESCRIPTION' => 'Resolution Description',
  'LBL_EDITVIEW_PANEL1' => 'New Panel 1',
  'LBL_AOS_PRODUCT_CATEGORIES_BUGS_1_FROM_AOS_PRODUCT_CATEGORIES_TITLE' => 'Product Category',
  'LBL_FOUND_IN_RELEASE' => 'Found in Revision',
  'LBL_FIXED_IN_RELEASE' => 'Fixed in Revision',
  'LBL_BUG_REQUIRED_CLOSE_DATE' => 'Required Close Date',
  'LBL_CASES_SUBPANEL_TITLE' => 'Cases',
);