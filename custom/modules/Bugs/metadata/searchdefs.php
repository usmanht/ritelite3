<?php
$searchdefs ['Bugs'] = 
array (
  'layout' => 
  array (
    'basic_search' => 
    array (
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'description' => 
      array (
        'type' => 'text',
        'label' => 'LBL_DESCRIPTION',
        'sortable' => false,
        'width' => '10%',
        'default' => true,
        'name' => 'description',
      ),
      'resolution_description_c' => 
      array (
        'type' => 'text',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_RESOLUTION_DESCRIPTION',
        'sortable' => false,
        'width' => '10%',
        'name' => 'resolution_description_c',
      ),
      'product_category' => 
      array (
        'type' => 'enum',
        'label' => 'LBL_PRODUCT_CATEGORY',
        'width' => '10%',
        'default' => true,
        'name' => 'product_category',
      ),
      'current_user_only' => 
      array (
        'name' => 'current_user_only',
        'label' => 'LBL_CURRENT_USER_FILTER',
        'type' => 'bool',
        'default' => true,
        'width' => '10%',
      ),
      'open_only' => 
      array (
        'name' => 'open_only',
        'label' => 'LBL_OPEN_ITEMS',
        'type' => 'bool',
        'default' => true,
        'width' => '10%',
      ),
      'aos_product_categories_bugs_1_name' => 
      array (
        'type' => 'relate',
        'link' => true,
        'label' => 'LBL_AOS_PRODUCT_CATEGORIES_BUGS_1_FROM_AOS_PRODUCT_CATEGORIES_TITLE',
        'id' => 'AOS_PRODUCT_CATEGORIES_BUGS_1AOS_PRODUCT_CATEGORIES_IDA',
        'width' => '10%',
        'default' => true,
        'name' => 'aos_product_categories_bugs_1_name',
      ),
      'bug_required_close_date_c' => 
      array (
        'type' => 'date',
        'default' => true,
        'label' => 'LBL_BUG_REQUIRED_CLOSE_DATE',
        'width' => '10%',
        'name' => 'bug_required_close_date_c',
      ),
    ),
    'advanced_search' => 
    array (
      'bug_number' => 
      array (
        'name' => 'bug_number',
        'default' => true,
        'width' => '10%',
      ),
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'description' => 
      array (
        'type' => 'text',
        'label' => 'LBL_DESCRIPTION',
        'sortable' => false,
        'width' => '10%',
        'default' => true,
        'name' => 'description',
      ),
      'resolution' => 
      array (
        'name' => 'resolution',
        'default' => true,
        'width' => '10%',
      ),
      'resolution_description_c' => 
      array (
        'type' => 'text',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_RESOLUTION_DESCRIPTION',
        'sortable' => false,
        'width' => '10%',
        'name' => 'resolution_description_c',
      ),
      'aos_product_categories_bugs_1_name' => 
      array (
        'type' => 'relate',
        'link' => true,
        'label' => 'LBL_AOS_PRODUCT_CATEGORIES_BUGS_1_FROM_AOS_PRODUCT_CATEGORIES_TITLE',
        'width' => '10%',
        'default' => true,
        'id' => 'AOS_PRODUCT_CATEGORIES_BUGS_1AOS_PRODUCT_CATEGORIES_IDA',
        'name' => 'aos_product_categories_bugs_1_name',
      ),
      'bug_required_close_date_c' => 
      array (
        'type' => 'date',
        'default' => true,
        'label' => 'LBL_BUG_REQUIRED_CLOSE_DATE',
        'width' => '10%',
        'name' => 'bug_required_close_date_c',
      ),
      'found_in_release' => 
      array (
        'name' => 'found_in_release',
        'default' => true,
        'width' => '10%',
      ),
      'fixed_in_release' => 
      array (
        'name' => 'fixed_in_release',
        'default' => true,
        'width' => '10%',
      ),
      'type' => 
      array (
        'name' => 'type',
        'default' => true,
        'width' => '10%',
      ),
      'source' => 
      array (
        'type' => 'enum',
        'label' => 'LBL_SOURCE',
        'width' => '10%',
        'default' => true,
        'name' => 'source',
      ),
      'product_category' => 
      array (
        'type' => 'enum',
        'label' => 'LBL_PRODUCT_CATEGORY',
        'width' => '10%',
        'default' => true,
        'name' => 'product_category',
      ),
      'status' => 
      array (
        'name' => 'status',
        'default' => true,
        'width' => '10%',
      ),
      'assigned_user_id' => 
      array (
        'name' => 'assigned_user_id',
        'type' => 'enum',
        'label' => 'LBL_ASSIGNED_TO',
        'function' => 
        array (
          'name' => 'get_user_array',
          'params' => 
          array (
            0 => false,
          ),
        ),
        'default' => true,
        'width' => '10%',
      ),
      'created_by' => 
      array (
        'type' => 'assigned_user_name',
        'label' => 'LBL_CREATED',
        'width' => '10%',
        'default' => true,
        'name' => 'created_by',
      ),
      'priority' => 
      array (
        'name' => 'priority',
        'default' => true,
        'width' => '10%',
      ),
      'current_user_only' => 
      array (
        'label' => 'LBL_CURRENT_USER_FILTER',
        'type' => 'bool',
        'default' => true,
        'width' => '10%',
        'name' => 'current_user_only',
      ),
      'open_only' => 
      array (
        'label' => 'LBL_OPEN_ITEMS',
        'type' => 'bool',
        'default' => true,
        'width' => '10%',
        'name' => 'open_only',
      ),
      'date_entered' => 
      array (
        'type' => 'datetime',
        'label' => 'LBL_DATE_ENTERED',
        'width' => '10%',
        'default' => true,
        'name' => 'date_entered',
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'maxColumnsBasic' => '4',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
  ),
);
?>
