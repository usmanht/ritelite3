<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2014-08-26 09:34:04
$dictionary["Call"]["fields"]["aos_quotes_calls_1"] = array (
  'name' => 'aos_quotes_calls_1',
  'type' => 'link',
  'relationship' => 'aos_quotes_calls_1',
  'source' => 'non-db',
  'module' => 'AOS_Quotes',
  'bean_name' => 'AOS_Quotes',
  'vname' => 'LBL_AOS_QUOTES_CALLS_1_FROM_AOS_QUOTES_TITLE',
  'id_name' => 'aos_quotes_calls_1aos_quotes_ida',
);
$dictionary["Call"]["fields"]["aos_quotes_calls_1_name"] = array (
  'name' => 'aos_quotes_calls_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_AOS_QUOTES_CALLS_1_FROM_AOS_QUOTES_TITLE',
  'save' => true,
  'id_name' => 'aos_quotes_calls_1aos_quotes_ida',
  'link' => 'aos_quotes_calls_1',
  'table' => 'aos_quotes',
  'module' => 'AOS_Quotes',
  'rname' => 'name',
);
$dictionary["Call"]["fields"]["aos_quotes_calls_1aos_quotes_ida"] = array (
  'name' => 'aos_quotes_calls_1aos_quotes_ida',
  'type' => 'link',
  'relationship' => 'aos_quotes_calls_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_AOS_QUOTES_CALLS_1_FROM_CALLS_TITLE',
);


/**
 * Created by JetBrains PhpStorm.
 * User: andrew
 * Date: 01/03/13
 * Time: 15:13
 * To change this template use File | Settings | File Templates.
 */

$dictionary['Call']['fields']['reschedule_history'] = array(

    'required' => false,
    'name' => 'reschedule_history',
    'vname' => 'LBL_RESCHEDULE_HISTORY',
    'type' => 'varchar',
    'source' => 'non-db',
    'studio' => 'visible',
    'massupdate' => 0,
    'importable' => 'false',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => 0,
    'audited' => false,
    'reportable' => false,
    'function' =>
    array (
        'name' => 'reschedule_history',
        'returns' => 'html',
        'include' => 'custom/modules/Calls/reschedule_history.php'
    ),
);

$dictionary['Call']['fields']['reschedule_count'] = array(

    'required' => false,
    'name' => 'reschedule_count',
    'vname' => 'LBL_RESCHEDULE_COUNT',
    'type' => 'varchar',
    'source' => 'non-db',
    'studio' => 'visible',
    'massupdate' => 0,
    'importable' => 'false',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => 0,
    'audited' => false,
    'reportable' => false,
    'function' =>
    array (
        'name' => 'reschedule_count',
        'returns' => 'html',
        'include' => 'custom/modules/Calls/reschedule_history.php'
    ),
);

// created: 2010-12-20 02:55:45
$dictionary["Call"]["fields"]["calls_reschedule"] = array (
    'name' => 'calls_reschedule',
    'type' => 'link',
    'relationship' => 'calls_reschedule',
    'module'=>'Calls_Reschedule',
    'bean_name'=>'Calls_Reschedule',
    'source'=>'non-db',
);


// created: 2010-12-20 02:56:01
$dictionary["Call"]["relationships"]["calls_reschedule"] = array (
    'lhs_module'=> 'Calls',
    'lhs_table'=> 'calls',
    'lhs_key' => 'id',
    'rhs_module'=> 'Calls_Reschedule',
    'rhs_table'=> 'calls_reschedule',
    'rhs_key' => 'call_id',
    'relationship_type'=>'one-to-many',
);


// created: 2014-08-26 12:19:38
$dictionary["Call"]["fields"]["opportunities_calls_1"] = array (
  'name' => 'opportunities_calls_1',
  'type' => 'link',
  'relationship' => 'opportunities_calls_1',
  'source' => 'non-db',
  'module' => 'Opportunities',
  'bean_name' => 'Opportunity',
  'vname' => 'LBL_OPPORTUNITIES_CALLS_1_FROM_OPPORTUNITIES_TITLE',
  'id_name' => 'opportunities_calls_1opportunities_ida',
);
$dictionary["Call"]["fields"]["opportunities_calls_1_name"] = array (
  'name' => 'opportunities_calls_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_OPPORTUNITIES_CALLS_1_FROM_OPPORTUNITIES_TITLE',
  'save' => true,
  'id_name' => 'opportunities_calls_1opportunities_ida',
  'link' => 'opportunities_calls_1',
  'table' => 'opportunities',
  'module' => 'Opportunities',
  'rname' => 'name',
);
$dictionary["Call"]["fields"]["opportunities_calls_1opportunities_ida"] = array (
  'name' => 'opportunities_calls_1opportunities_ida',
  'type' => 'link',
  'relationship' => 'opportunities_calls_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_OPPORTUNITIES_CALLS_1_FROM_CALLS_TITLE',
);



$dictionary['Call']['fields']['SecurityGroups'] = array (
  	'name' => 'SecurityGroups',
    'type' => 'link',
	'relationship' => 'securitygroups_calls',
	'module'=>'SecurityGroups',
	'bean_name'=>'SecurityGroup',
    'source'=>'non-db',
	'vname'=>'LBL_SECURITYGROUPS',
);






 // created: 2014-08-26 09:18:46

 

 // created: 2014-08-26 09:18:46
$dictionary['Call']['fields']['account_name_c']['labelValue']='Account Name';

 

 // created: 2014-09-12 13:47:18
$dictionary['Call']['fields']['call_notes_c']['labelValue']='call notes';

 

 // created: 2015-10-21 10:36:48
$dictionary['Call']['fields']['status']['massupdate']='1';
$dictionary['Call']['fields']['status']['comments']='The status of the call (Held, Not Held, etc.)';
$dictionary['Call']['fields']['status']['merge_filter']='disabled';

 
?>