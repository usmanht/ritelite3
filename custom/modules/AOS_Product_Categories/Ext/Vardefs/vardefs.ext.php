<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2018-01-11 10:37:23
$dictionary["AOS_Product_Categories"]["fields"]["aos_product_categories_bugs_1"] = array (
  'name' => 'aos_product_categories_bugs_1',
  'type' => 'link',
  'relationship' => 'aos_product_categories_bugs_1',
  'source' => 'non-db',
  'module' => 'Bugs',
  'bean_name' => 'Bug',
  'side' => 'right',
  'vname' => 'LBL_AOS_PRODUCT_CATEGORIES_BUGS_1_FROM_BUGS_TITLE',
);


// created: 2018-01-12 08:18:24
$dictionary["AOS_Product_Categories"]["fields"]["aos_product_categories_opportunities_1"] = array (
  'name' => 'aos_product_categories_opportunities_1',
  'type' => 'link',
  'relationship' => 'aos_product_categories_opportunities_1',
  'source' => 'non-db',
  'module' => 'Opportunities',
  'bean_name' => 'Opportunity',
  'side' => 'right',
  'vname' => 'LBL_AOS_PRODUCT_CATEGORIES_OPPORTUNITIES_1_FROM_OPPORTUNITIES_TITLE',
);


// created: 2014-06-03 10:30:00
$dictionary["AOS_Product_Categories"]["fields"]["documents_aos_product_categories_1"] = array (
  'name' => 'documents_aos_product_categories_1',
  'type' => 'link',
  'relationship' => 'documents_aos_product_categories_1',
  'source' => 'non-db',
  'module' => 'Documents',
  'bean_name' => 'Document',
  'vname' => 'LBL_DOCUMENTS_AOS_PRODUCT_CATEGORIES_1_FROM_DOCUMENTS_TITLE',
);

?>