<?php
$popupMeta = array (
    'moduleMain' => 'Prospect',
    'varName' => 'PROSPECT',
    'orderBy' => 'prospects.last_name, prospects.first_name',
    'whereClauses' => array (
  'first_name' => 'prospects.first_name',
  'last_name' => 'prospects.last_name',
  'phone' => 'prospects.phone',
  'email' => 'prospects.email',
  'assistant' => 'prospects.assistant',
  'do_not_call' => 'prospects.do_not_call',
  'account_name' => 'prospects.account_name',
  'companyname_c' => 'prospects_cstm.companyname_c',
  'address_street' => 'prospects.address_street',
  'address_state' => 'prospects.address_state',
  'address_postalcode' => 'prospects.address_postalcode',
  'primary_address_country' => 'prospects.primary_address_country',
  'assigned_user_name' => 'prospects.assigned_user_name',
  'accesscontactid_c' => 'prospects_cstm.accesscontactid_c',
  'searchlabel_c' => 'prospects_cstm.searchlabel_c',
  'search_tags_c' => 'prospects_cstm.search_tags_c',
),
    'searchInputs' => array (
  0 => 'first_name',
  1 => 'last_name',
  2 => 'phone',
  3 => 'email',
  4 => 'assistant',
  5 => 'do_not_call',
  6 => 'account_name',
  7 => 'companyname_c',
  8 => 'address_street',
  9 => 'address_state',
  10 => 'address_postalcode',
  11 => 'primary_address_country',
  12 => 'assigned_user_name',
  13 => 'accesscontactid_c',
  14 => 'searchlabel_c',
  15 => 'search_tags_c',
),
    'create' => array (
  'formBase' => 'ProspectFormBase.php',
  'formBaseClass' => 'ProspectFormBase',
  'getFormBodyParams' => 
  array (
    0 => '',
    1 => '',
    2 => 'ProspectSave',
  ),
  'createButton' => 'LNK_NEW_PROSPECT',
),
    'searchdefs' => array (
  'first_name' => 
  array (
    'name' => 'first_name',
    'width' => '10%',
  ),
  'last_name' => 
  array (
    'name' => 'last_name',
    'width' => '10%',
  ),
  'phone' => 
  array (
    'name' => 'phone',
    'label' => 'LBL_ANY_PHONE',
    'type' => 'name',
    'width' => '10%',
  ),
  'email' => 
  array (
    'name' => 'email',
    'label' => 'LBL_ANY_EMAIL',
    'type' => 'name',
    'width' => '10%',
  ),
  'assistant' => 
  array (
    'name' => 'assistant',
    'width' => '10%',
  ),
  'do_not_call' => 
  array (
    'name' => 'do_not_call',
    'width' => '10%',
  ),
  'account_name' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_ACCOUNT_NAME',
    'width' => '10%',
    'name' => 'account_name',
  ),
  'companyname_c' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_COMPANYNAME',
    'width' => '10%',
    'name' => 'companyname_c',
  ),
  'address_street' => 
  array (
    'name' => 'address_street',
    'label' => 'LBL_ANY_ADDRESS',
    'type' => 'name',
    'width' => '10%',
  ),
  'address_state' => 
  array (
    'name' => 'address_state',
    'label' => 'LBL_STATE',
    'type' => 'name',
    'width' => '10%',
  ),
  'address_postalcode' => 
  array (
    'name' => 'address_postalcode',
    'label' => 'LBL_POSTAL_CODE',
    'type' => 'name',
    'width' => '10%',
  ),
  'primary_address_country' => 
  array (
    'name' => 'primary_address_country',
    'label' => 'LBL_COUNTRY',
    'type' => 'name',
    'options' => 'countries_dom',
    'width' => '10%',
  ),
  'assigned_user_name' => 
  array (
    'link' => true,
    'type' => 'relate',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'id' => 'ASSIGNED_USER_ID',
    'width' => '10%',
    'name' => 'assigned_user_name',
  ),
  'accesscontactid_c' => 
  array (
    'type' => 'int',
    'label' => 'LBL_ACCESSCONTACTID',
    'width' => '10%',
    'name' => 'accesscontactid_c',
  ),
  'searchlabel_c' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_SEARCHLABEL',
    'width' => '10%',
    'name' => 'searchlabel_c',
  ),
  'search_tags_c' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_SEARCH_TAGS',
    'width' => '10%',
    'name' => 'search_tags_c',
  ),
),
    'listviewdefs' => array (
  'FULL_NAME' => 
  array (
    'width' => '20%',
    'label' => 'LBL_LIST_NAME',
    'link' => true,
    'related_fields' => 
    array (
      0 => 'first_name',
      1 => 'last_name',
    ),
    'orderBy' => 'last_name',
    'default' => true,
    'name' => 'full_name',
  ),
  'TITLE' => 
  array (
    'width' => '20%',
    'label' => 'LBL_LIST_TITLE',
    'link' => false,
    'default' => true,
    'name' => 'title',
  ),
  'ACCOUNT_NAME' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_ACCOUNT_NAME',
    'width' => '10%',
    'default' => true,
  ),
  'COMPANYNAME_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_COMPANYNAME',
    'width' => '34%',
    'name' => 'companyname_c',
  ),
  'PHONE_WORK' => 
  array (
    'width' => '10%',
    'label' => 'LBL_LIST_PHONE',
    'link' => false,
    'default' => true,
    'name' => 'phone_work',
  ),
  'PHONE_MOBILE' => 
  array (
    'type' => 'phone',
    'label' => 'LBL_MOBILE_PHONE',
    'width' => '10%',
    'default' => true,
    'name' => 'phone_mobile',
  ),
  'EMAIL1' => 
  array (
    'width' => '20%',
    'label' => 'LBL_LIST_EMAIL_ADDRESS',
    'sortable' => false,
    'link' => false,
    'default' => true,
    'name' => 'email1',
  ),
  'WEBSITE_C' => 
  array (
    'type' => 'url',
    'default' => true,
    'label' => 'LBL_WEBSITE',
    'width' => '10%',
    'name' => 'website_c',
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'link' => true,
    'type' => 'relate',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'id' => 'ASSIGNED_USER_ID',
    'width' => '10%',
    'default' => true,
    'name' => 'assigned_user_name',
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => true,
    'name' => 'date_entered',
  ),
),
);
