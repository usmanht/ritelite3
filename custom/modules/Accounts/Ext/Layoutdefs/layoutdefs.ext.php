<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2014-05-07 16:14:02
$layout_defs["Accounts"]["subpanel_setup"]['accounts_aos_invoices_1'] = array (
  'order' => 100,
  'module' => 'AOS_Invoices',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_AOS_INVOICES_1_FROM_AOS_INVOICES_TITLE',
  'get_subpanel_data' => 'accounts_aos_invoices_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2014-03-25 16:57:28
$layout_defs["Accounts"]["subpanel_setup"]['accounts_aos_quotes_1'] = array (
  'order' => 100,
  'module' => 'AOS_Quotes',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_AOS_QUOTES_1_FROM_AOS_QUOTES_TITLE',
  'get_subpanel_data' => 'accounts_aos_quotes_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2014-05-07 16:09:41
$layout_defs["Accounts"]["subpanel_setup"]['accounts_rl_sales_order_line_items_1'] = array (
  'order' => 100,
  'module' => 'RL_Sales_Order_Line_Items',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_RL_SALES_ORDER_LINE_ITEMS_1_FROM_RL_SALES_ORDER_LINE_ITEMS_TITLE',
  'get_subpanel_data' => 'accounts_rl_sales_order_line_items_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2014-05-07 16:11:42
$layout_defs["Accounts"]["subpanel_setup"]['accounts_rl_sales_order_line_items_2'] = array (
  'order' => 100,
  'module' => 'RL_Sales_Order_Line_Items',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_RL_SALES_ORDER_LINE_ITEMS_2_FROM_RL_SALES_ORDER_LINE_ITEMS_TITLE',
  'get_subpanel_data' => 'accounts_rl_sales_order_line_items_2',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);



/*$layout_defs['Accounts']['subpanel_setup']['accounts_rl_saleshistory_1']['top_buttons']= array(
    array('widget_class' => 'SubPanelTopCreateButton'),
    array('widget_class' => 'SubPanelTopSelectButton', 'mode'=>'MultiSelect'),
    array('widget_class' => 'SubPanelTopFilterInputButton'),
);*/

$layout_defs['Accounts']['subpanel_setup']['accounts_rl_saleshistory_1']['top_buttons'][] = array('widget_class' => 'SubPanelSearch');

unset($layout_defs['Accounts']['subpanel_setup']['accounts_rl_saleshistory_1']['top_buttons'][1]);


/**
 * Advanced OpenSales, Advanced, robust set of sales modules.
 * @package Advanced OpenSales for SugarCRM
 * @copyright SalesAgility Ltd http://www.salesagility.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU AFFERO GENERAL PUBLIC LICENSE
 * along with this program; if not, see http://www.gnu.org/licenses
 * or write to the Free Software Foundation,Inc., 51 Franklin Street,
 * Fifth Floor, Boston, MA 02110-1301  USA
 *
 * @author SalesAgility <info@salesagility.com>
 */

$layout_defs["Accounts"]["subpanel_setup"]["account_aos_quotes"] = array (
  'order' => 100,
  'module' => 'AOS_Quotes',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'AOS_Quotes',
  'get_subpanel_data' => 'aos_quotes',
);

$layout_defs["Accounts"]["subpanel_setup"]["account_aos_invoices"] = array (
  'order' => 100,
  'module' => 'AOS_Invoices',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'AOS_Invoices',
  'get_subpanel_data' => 'aos_invoices',
);

$layout_defs["Accounts"]["subpanel_setup"]["account_aos_contracts"] = array (
  'order' => 100,
  'module' => 'AOS_Contracts',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'AOS_Contracts',
  'get_subpanel_data' => 'aos_contracts',
);





$layout_defs['Accounts']['subpanel_setup']['securitygroups'] = array(
	'top_buttons' => array(array('widget_class' => 'SubPanelTopSelectButton', 'popup_module' => 'SecurityGroups', 'mode' => 'MultiSelect'),),
	'order' => 900,
	'sort_by' => 'name',
	'sort_order' => 'asc',
	'module' => 'SecurityGroups',
	'refresh_page'=>1,
	'subpanel_name' => 'default',
	'get_subpanel_data' => 'SecurityGroups',
	'add_subpanel_data' => 'securitygroup_id',
	'title_key' => 'LBL_SECURITYGROUPS_SUBPANEL_TITLE',
);






//auto-generated file DO NOT EDIT
$layout_defs['Accounts']['subpanel_setup']['accounts_aos_invoices_1']['override_subpanel_name'] = 'Account_subpanel_accounts_aos_invoices_1';


//auto-generated file DO NOT EDIT
$layout_defs['Accounts']['subpanel_setup']['accounts_aos_quotes_1']['override_subpanel_name'] = 'Account_subpanel_accounts_aos_quotes_1';


//auto-generated file DO NOT EDIT
$layout_defs['Accounts']['subpanel_setup']['accounts_rl_saleshistory_1']['override_subpanel_name'] = 'Account_subpanel_accounts_rl_saleshistory_1';


//auto-generated file DO NOT EDIT
$layout_defs['Accounts']['subpanel_setup']['accounts_rl_sales_order_line_items_1']['override_subpanel_name'] = 'Account_subpanel_accounts_rl_sales_order_line_items_1';


//auto-generated file DO NOT EDIT
$layout_defs['Accounts']['subpanel_setup']['accounts_rl_sales_order_line_items_2']['override_subpanel_name'] = 'Account_subpanel_accounts_rl_sales_order_line_items_2';


//auto-generated file DO NOT EDIT
$layout_defs['Accounts']['subpanel_setup']['account_aos_contracts']['override_subpanel_name'] = 'Account_subpanel_account_aos_contracts';


//auto-generated file DO NOT EDIT
$layout_defs['Accounts']['subpanel_setup']['account_aos_invoices']['override_subpanel_name'] = 'Account_subpanel_account_aos_invoices';


//auto-generated file DO NOT EDIT
$layout_defs['Accounts']['subpanel_setup']['account_aos_quotes']['override_subpanel_name'] = 'Account_subpanel_account_aos_quotes';


//auto-generated file DO NOT EDIT
$layout_defs['Accounts']['subpanel_setup']['campaigns']['override_subpanel_name'] = 'Account_subpanel_campaigns';


//auto-generated file DO NOT EDIT
$layout_defs['Accounts']['subpanel_setup']['contacts']['override_subpanel_name'] = 'Account_subpanel_contacts';


//auto-generated file DO NOT EDIT
$layout_defs['Accounts']['subpanel_setup']['documents']['override_subpanel_name'] = 'Account_subpanel_documents';


//auto-generated file DO NOT EDIT
$layout_defs['Accounts']['subpanel_setup']['leads']['override_subpanel_name'] = 'Account_subpanel_leads';


//auto-generated file DO NOT EDIT
$layout_defs['Accounts']['subpanel_setup']['opportunities']['override_subpanel_name'] = 'Account_subpanel_opportunities';

?>