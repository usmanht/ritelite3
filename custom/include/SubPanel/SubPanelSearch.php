<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2012 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/




global $beanList;
global $beanFiles;
global $timedate;



if(empty($_REQUEST['module']))
{
	die("'module' was not defined");
}

if(empty($_REQUEST['record']))
{
	die("'record' was not defined");
}

if(!isset($beanList[$_REQUEST['module']]))
{
	die("'".$_REQUEST['module']."' is not defined in \$beanList");
}
$search_query = '';
$mod_limit = '';

$ddate = trim($_REQUEST['date']);

if(!empty($_REQUEST['date_range'])){
	if($_REQUEST['date_range'] == 'between'){
		if(!empty($_REQUEST['date_start']) && !empty($_REQUEST['date_end'])){
			$query_array['where'][] = 'sub_module.order_date '.$_REQUEST['date_range'].' \''.$timedate->to_db_date($_REQUEST['date_start'], false) .'\' and \''.$timedate->to_db_date($_REQUEST['date_end'], false).'\'';
		}
	} 
	else if($ddate){

		if($_REQUEST['date_range'] == 'equals'){
			$query_array['where'][] = 'date(sub_module.order_date) = \''.$timedate->to_db_date($_REQUEST['date'], false).'\'';
		} else if($_REQUEST['date_range'] == 'after'){
			$query_array['where'][] = 'sub_module.order_date > \''.$timedate->to_db_date($_REQUEST['date'], false).'\'';
		} else if($_REQUEST['date_range'] == 'before'){
			$query_array['where'][] = 'sub_module.order_date < \''.$timedate->to_db_date($_REQUEST['date'], false).'\'';
		}
	}	
	/*}else {
		$query_array['where'][] = 'sub_module.date_entered BETWEEN DATE_ADD(CURDATE()) AND DATE_ADD(CURDATE(), INTERVAL '.$_REQUEST['date_range'].' DAY)';
	}*/
}
//add search on total value
if(!empty($_REQUEST['date_range']) && !empty($_REQUEST['amount_range'])){
    if($_REQUEST['amount_range'] == 'between'){
        if(!empty($_REQUEST['total_amount1']) && !empty($_REQUEST['total_amount2'])){
            $query_array['where'][] = 'sub_module.total_value >= '.$_REQUEST['total_amount1'].' AND sub_module.total_value <= '.$_REQUEST['total_amount2'].'';
        }
    }
    else if(!empty($_REQUEST['amount'])){
        if($_REQUEST['amount_range'] == 'equals'){
            $query_array['where'][] = 'sub_module.total_value = \''.$_REQUEST['amount'].'\'';
        }
        else if($_REQUEST['amount_range'] == 'greater'){
            $query_array['where'][] = 'sub_module.total_value > \''.$_REQUEST['amount'].'\'';
        }
        else if($_REQUEST['amount_range'] == 'before'){
            $query_array['where'][] = 'sub_module.total_value < \''.$_REQUEST['amount'].'\'';
        }
    }
}


if(isset($query_array['where'])){
    $query_where = '';
    foreach ($query_array['where'] as $where){
        $query_where .=  ($query_where == '' ? '' : ' AND ').$where;
    }
    $search_query .= ' '.$query_where;
}


// $search_query;

$subpanel = $_REQUEST['subpanel'];
$record = $_REQUEST['record'];
$module = $_REQUEST['module'];

if(empty($_REQUEST['inline']))
{
	insert_popup_header($theme);
}

//require_once('include/SubPanel/SubPanelDefinitions.php');
//require_once($beanFiles[$beanList[$_REQUEST['module']]]);
//$focus=new $beanList[$_REQUEST['module']];
//$focus->retrieve($record);

include('custom/include/SubPanel/SearchSubPanel.php');
$layout_def_key = '';
if(!empty($_REQUEST['layout_def_key'])){
	$layout_def_key = $_REQUEST['layout_def_key'];
}

$subpanel_object = new SearchSubPanel($module, $record, $subpanel,null, $layout_def_key,$search_query,$mod_limit);

$subpanel_object->setTemplateFile('include/SubPanel/SubPanelDynamic.html');
echo (empty($_REQUEST['inline']))?$subpanel_object->get_buttons():'' ;

$subpanel_object->display();

if(empty($_REQUEST['inline']))
{
	insert_popup_footer($theme);
}

?>
