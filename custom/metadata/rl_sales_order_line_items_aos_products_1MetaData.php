<?php
// created: 2015-05-28 17:21:59
$dictionary["rl_sales_order_line_items_aos_products_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'rl_sales_order_line_items_aos_products_1' => 
    array (
      'lhs_module' => 'RL_Sales_Order_Line_Items',
      'lhs_table' => 'rl_sales_order_line_items',
      'lhs_key' => 'id',
      'rhs_module' => 'AOS_Products',
      'rhs_table' => 'aos_products',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'rl_sales_order_line_items_aos_products_1_c',
      'join_key_lhs' => 'rl_sales_order_line_items_aos_products_1rl_sales_order_line_items_ida',
      'join_key_rhs' => 'rl_sales_order_line_items_aos_products_1aos_products_idb',
    ),
  ),
  'table' => 'rl_sales_order_line_items_aos_products_1_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'rl_sales_order_line_items_aos_products_1rl_sales_order_line_items_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'rl_sales_order_line_items_aos_products_1aos_products_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'rl_sales_order_line_items_aos_products_1spk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'rl_sales_order_line_items_aos_products_1_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'rl_sales_order_line_items_aos_products_1rl_sales_order_line_items_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'rl_sales_order_line_items_aos_products_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'rl_sales_order_line_items_aos_products_1aos_products_idb',
      ),
    ),
  ),
);