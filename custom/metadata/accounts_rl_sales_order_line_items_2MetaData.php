<?php
// created: 2014-05-07 16:11:42
$dictionary["accounts_rl_sales_order_line_items_2"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'accounts_rl_sales_order_line_items_2' => 
    array (
      'lhs_module' => 'Accounts',
      'lhs_table' => 'accounts',
      'lhs_key' => 'id',
      'rhs_module' => 'RL_Sales_Order_Line_Items',
      'rhs_table' => 'rl_sales_order_line_items',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'accounts_rl_sales_order_line_items_2_c',
      'join_key_lhs' => 'accounts_rl_sales_order_line_items_2accounts_ida',
      'join_key_rhs' => 'accounts_rl_sales_order_line_items_2rl_sales_order_line_items_idb',
    ),
  ),
  'table' => 'accounts_rl_sales_order_line_items_2_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'accounts_rl_sales_order_line_items_2accounts_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'accounts_rl_sales_order_line_items_2rl_sales_order_line_items_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'accounts_rl_sales_order_line_items_2spk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'accounts_rl_sales_order_line_items_2_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'accounts_rl_sales_order_line_items_2accounts_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'accounts_rl_sales_order_line_items_2_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'accounts_rl_sales_order_line_items_2rl_sales_order_line_items_idb',
      ),
    ),
  ),
);