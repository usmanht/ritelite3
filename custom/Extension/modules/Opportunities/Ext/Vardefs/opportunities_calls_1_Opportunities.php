<?php
// created: 2014-08-26 12:19:38
$dictionary["Opportunity"]["fields"]["opportunities_calls_1"] = array (
  'name' => 'opportunities_calls_1',
  'type' => 'link',
  'relationship' => 'opportunities_calls_1',
  'source' => 'non-db',
  'module' => 'Calls',
  'bean_name' => 'Call',
  'side' => 'right',
  'vname' => 'LBL_OPPORTUNITIES_CALLS_1_FROM_CALLS_TITLE',
);
