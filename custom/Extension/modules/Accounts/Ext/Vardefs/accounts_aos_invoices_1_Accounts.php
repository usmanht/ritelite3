<?php
// created: 2014-05-07 16:14:02
$dictionary["Account"]["fields"]["accounts_aos_invoices_1"] = array (
  'name' => 'accounts_aos_invoices_1',
  'type' => 'link',
  'relationship' => 'accounts_aos_invoices_1',
  'source' => 'non-db',
  'module' => 'AOS_Invoices',
  'bean_name' => 'AOS_Invoices',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_AOS_INVOICES_1_FROM_AOS_INVOICES_TITLE',
);
