<?php
// created: 2014-04-11 12:48:55
$dictionary["AOS_Products"]["fields"]["documents_aos_products_1"] = array (
  'name' => 'documents_aos_products_1',
  'type' => 'link',
  'relationship' => 'documents_aos_products_1',
  'source' => 'non-db',
  'module' => 'Documents',
  'bean_name' => 'Document',
  'vname' => 'LBL_DOCUMENTS_AOS_PRODUCTS_1_FROM_DOCUMENTS_TITLE',
);
