<?php 
 // created: 2014-03-21 16:58:48
$mod_strings['LBL_NAME'] = 'Stock Code';
$mod_strings['LBL_PRICE'] = 'List Price';
$mod_strings['LBL_COST'] = 'Cost Price';
$mod_strings['LBL_LOWEST_SALE_PRICE'] = 'Lowest Sale Price';
$mod_strings['LBL_QUANTITY'] = 'Quantity in Stock';
$mod_strings['LBL_BACK_ORDER_QUANTITY'] = 'Quantity on Back Order ';
$mod_strings['LBL_NINETY_DAYS_SALES'] = '90 Days Sales Figure';
$mod_strings['LBL_CLASSIFICATION'] = 'Classification';
$mod_strings['LBL_COMMODITY_CODE'] = 'Commodity Code';

?>
