<?php
// created: 2014-05-07 16:14:02
$dictionary["AOS_Invoices"]["fields"]["accounts_aos_invoices_1"] = array (
  'name' => 'accounts_aos_invoices_1',
  'type' => 'link',
  'relationship' => 'accounts_aos_invoices_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_ACCOUNTS_AOS_INVOICES_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_aos_invoices_1accounts_ida',
);
$dictionary["AOS_Invoices"]["fields"]["accounts_aos_invoices_1_name"] = array (
  'name' => 'accounts_aos_invoices_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_AOS_INVOICES_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_aos_invoices_1accounts_ida',
  'link' => 'accounts_aos_invoices_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["AOS_Invoices"]["fields"]["accounts_aos_invoices_1accounts_ida"] = array (
  'name' => 'accounts_aos_invoices_1accounts_ida',
  'type' => 'link',
  'relationship' => 'accounts_aos_invoices_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_AOS_INVOICES_1_FROM_AOS_INVOICES_TITLE',
);
