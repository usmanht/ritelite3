<?php
 // created: 2014-04-23 11:03:01
$dictionary['AOS_Invoices']['fields']['quote_number']['options']='numeric_range_search_dom';
$dictionary['AOS_Invoices']['fields']['quote_number']['merge_filter']='disabled';
$dictionary['AOS_Invoices']['fields']['quote_number']['enable_range_search']='1';
$dictionary['AOS_Invoices']['fields']['quote_number']['min']=false;
$dictionary['AOS_Invoices']['fields']['quote_number']['max']=false;
$dictionary['AOS_Invoices']['fields']['quote_number']['disable_num_format']='1';

 ?>