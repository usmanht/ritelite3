<?php
// created: 2014-05-08 10:48:41
$dictionary["AOS_Invoices"]["fields"]["aos_products_aos_invoices_1"] = array (
  'name' => 'aos_products_aos_invoices_1',
  'type' => 'link',
  'relationship' => 'aos_products_aos_invoices_1',
  'source' => 'non-db',
  'module' => 'AOS_Products',
  'bean_name' => 'AOS_Products',
  'vname' => 'LBL_AOS_PRODUCTS_AOS_INVOICES_1_FROM_AOS_PRODUCTS_TITLE',
);
