<?php
$dictionary["AOS_Invoices"]["fields"]["sage_ref_billing"] = array(
    'name' => 'sage_ref_billing',
    'vname' => 'LBL_SAGE_REF',
    'comments' => '',
    'help' => '',
    'module' => 'AOS_Invoices',
    'type' => 'varchar',
    'max_size' => '155',
    'reportable' => '0',
   // 'source' => 'non-db',
    //'dbtype' => 'non-db',
    'studio' => 'visible'
);

$dictionary["AOS_Invoices"]["fields"]["sage_ref_shipping"] = array(
    'name' => 'sage_ref_shipping',
    'vname' => 'LBL_SAGE_REF1',
    'comments' => '',
    'help' => '',
    'module' => 'AOS_Invoices',
    'type' => 'varchar',
    'max_size' => '155',
    'reportable' => '0',
   // 'source' => 'non-db',
    //'dbtype' => 'non-db',
    'studio' => 'visible'
);