<?php 
 // created: 2014-05-07 13:44:40
$mod_strings['LNK_NEW_RECORD'] = 'Create Sales Order';
$mod_strings['LNK_LIST'] = 'View Sales Orders';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'Creating this account may potentially create a duplicate account. You may either click on Save to continue creating this new account with the previously entered data or you may click Cancel.';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Sales Order List';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Search Sales Order';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'My Sales Orders';
$mod_strings['LBL_PAYMENT_TERMS'] = 'Payment Terms';
$mod_strings['LBL_REQUESTED_DELIVERY_DATE'] = 'Requested Delivery Date';
$mod_strings['LBL_SHIPPING_METHOD'] = 'Shipping Method';
$mod_strings['LBL_BILLING_ACCOUNT'] = 'Billing Account';
$mod_strings['LBL_BILLING_CONTACT'] = 'Billing Contact';
$mod_strings['LBL_LIST_NUM'] = 'Sales Order Number';
$mod_strings['LBL_DATE_DESPATCHED'] = 'Despatch Date';
$mod_strings['LBL_QUOTE_DATE'] = 'Order Date';
$mod_strings['LBL_SERVICE_LIST_PRICE'] = 'Price';
$mod_strings['LBL_QUOTE_NUMBER'] = 'Quote Number';
$mod_strings['LBL_DATE_DESPATCHED '] = 'Date Despatched';
$mod_strings['LBL_EMAIL_INVOICE'] = 'Email Sales Order';
$mod_strings['LBL_PRODUCT_LIST_PRICE_C'] = 'List Price';
$mod_strings['LBL_PRODUCT_UNIT_PRICE'] = 'Net Price Each';
$mod_strings['LBL_PRODUCT_LIST_PRICE'] = 'Price Each';
$mod_strings['LBL_DUE_DATE'] = 'Date Dispatched';
$mod_strings['LBL_PAYMENT_METHOD_LIST'] = 'Payment Method';
$mod_strings['LBL_SHIPPING_SERVICE_TERMS'] = 'Shipping Service/Terms';
$mod_strings['LBL_SO_NOTES_EXTERNAL'] = 'Notes for External Documents';
$mod_strings['LBL_TEST_CERTIFICATES_REQUIRED'] = 'Test Certificates Required';
$mod_strings['LBL_SO_NOTES_INTERNAL'] = 'Notes for Internal Use';
$mod_strings['LBL_EXPORT_DOCS_REQ'] = 'Export Documentation Required';
$mod_strings['LBL_INVOICE_NUMBER'] = 'Sales Order Number';
$mod_strings['LBL_INVOICE_NO'] = 'Invoice Number';
$mod_strings['LBL_EDITVIEW_PANEL1'] = 'Required Documentation';
$mod_strings['LBL_EDITVIEW_PANEL2'] = 'Notes for Internal Use Only';
$mod_strings['LBL_EDITVIEW_PANEL3'] = 'Notes for External Use';
$mod_strings['LBL_STATUS'] = 'Payment Status';
$mod_strings['LBL_SAGE_SOP_NUMBER'] = 'Sage SOP Number';
$mod_strings['LBL_TEMPLATE_DDOWN_C'] = 'Sales Order Templates';
$mod_strings['LBL_SALES_ORDER_STATUS'] = 'Sales Order Status';
$mod_strings['LBL_CUSTOMER_ORDER_NUMBER'] = 'Customer Order Number';
$mod_strings['LBL_INVOICE_DATE'] = 'Invoice Date';
$mod_strings['LBL_REQURIED_DESPATCH_DATE'] = 'Requried Despatch Date';

?>
