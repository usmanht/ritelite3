<?php
 // created: 2014-10-22 17:02:11
$dictionary['Bug']['fields']['status']['default']='New';
$dictionary['Bug']['fields']['status']['required']=true;
$dictionary['Bug']['fields']['status']['massupdate']='1';
$dictionary['Bug']['fields']['status']['comments']='The status of the issue';
$dictionary['Bug']['fields']['status']['merge_filter']='disabled';

 ?>