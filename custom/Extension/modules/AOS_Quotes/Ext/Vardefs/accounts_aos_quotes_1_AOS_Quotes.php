<?php
// created: 2014-03-25 16:57:28
$dictionary["AOS_Quotes"]["fields"]["accounts_aos_quotes_1"] = array (
  'name' => 'accounts_aos_quotes_1',
  'type' => 'link',
  'relationship' => 'accounts_aos_quotes_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_ACCOUNTS_AOS_QUOTES_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_aos_quotes_1accounts_ida',
);
$dictionary["AOS_Quotes"]["fields"]["accounts_aos_quotes_1_name"] = array (
  'name' => 'accounts_aos_quotes_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_AOS_QUOTES_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_aos_quotes_1accounts_ida',
  'link' => 'accounts_aos_quotes_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["AOS_Quotes"]["fields"]["accounts_aos_quotes_1accounts_ida"] = array (
  'name' => 'accounts_aos_quotes_1accounts_ida',
  'type' => 'link',
  'relationship' => 'accounts_aos_quotes_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_AOS_QUOTES_1_FROM_AOS_QUOTES_TITLE',
);
