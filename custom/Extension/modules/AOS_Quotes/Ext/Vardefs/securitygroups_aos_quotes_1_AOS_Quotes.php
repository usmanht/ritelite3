<?php
// created: 2014-05-02 12:12:37
$dictionary["AOS_Quotes"]["fields"]["securitygroups_aos_quotes_1"] = array (
  'name' => 'securitygroups_aos_quotes_1',
  'type' => 'link',
  'relationship' => 'securitygroups_aos_quotes_1',
  'source' => 'non-db',
  'module' => 'SecurityGroups',
  'bean_name' => 'SecurityGroup',
  'vname' => 'LBL_SECURITYGROUPS_AOS_QUOTES_1_FROM_SECURITYGROUPS_TITLE',
);
