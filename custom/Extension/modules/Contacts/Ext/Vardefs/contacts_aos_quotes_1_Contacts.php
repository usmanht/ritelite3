<?php
// created: 2014-03-25 17:07:24
$dictionary["Contact"]["fields"]["contacts_aos_quotes_1"] = array (
  'name' => 'contacts_aos_quotes_1',
  'type' => 'link',
  'relationship' => 'contacts_aos_quotes_1',
  'source' => 'non-db',
  'module' => 'AOS_Quotes',
  'bean_name' => 'AOS_Quotes',
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_AOS_QUOTES_1_FROM_AOS_QUOTES_TITLE',
);
