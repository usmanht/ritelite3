<?php
// created: 2014-01-07 11:53:04
$dictionary["Task"]["fields"]["users_tasks_1"] = array (
  'name' => 'users_tasks_1',
  'type' => 'link',
  'relationship' => 'users_tasks_1',
  'source' => 'non-db',
  'module' => 'Users',
  'bean_name' => 'User',
  'vname' => 'LBL_USERS_TASKS_1_FROM_USERS_TITLE',
  'id_name' => 'users_tasks_1users_ida',
);
$dictionary["Task"]["fields"]["users_tasks_1_name"] = array (
  'name' => 'users_tasks_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_USERS_TASKS_1_FROM_USERS_TITLE',
  'save' => true,
  'id_name' => 'users_tasks_1users_ida',
  'link' => 'users_tasks_1',
  'table' => 'users',
  'module' => 'Users',
  'rname' => 'name',
);
$dictionary["Task"]["fields"]["users_tasks_1users_ida"] = array (
  'name' => 'users_tasks_1users_ida',
  'type' => 'link',
  'relationship' => 'users_tasks_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_USERS_TASKS_1_FROM_TASKS_TITLE',
);
