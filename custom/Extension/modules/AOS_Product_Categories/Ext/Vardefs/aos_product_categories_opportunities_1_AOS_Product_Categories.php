<?php
// created: 2018-01-12 08:18:24
$dictionary["AOS_Product_Categories"]["fields"]["aos_product_categories_opportunities_1"] = array (
  'name' => 'aos_product_categories_opportunities_1',
  'type' => 'link',
  'relationship' => 'aos_product_categories_opportunities_1',
  'source' => 'non-db',
  'module' => 'Opportunities',
  'bean_name' => 'Opportunity',
  'side' => 'right',
  'vname' => 'LBL_AOS_PRODUCT_CATEGORIES_OPPORTUNITIES_1_FROM_OPPORTUNITIES_TITLE',
);
