<?php
// created: 2014-10-22 16:58:44
$dictionary["AOS_Products"]["fields"]["bugs_aos_products_1"] = array (
  'name' => 'bugs_aos_products_1',
  'type' => 'link',
  'relationship' => 'bugs_aos_products_1',
  'source' => 'non-db',
  'module' => 'Bugs',
  'bean_name' => 'Bug',
  'vname' => 'LBL_BUGS_AOS_PRODUCTS_1_FROM_BUGS_TITLE',
);
