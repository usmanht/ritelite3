<?php
// created: 2014-05-07 16:09:41
$dictionary["Account"]["fields"]["accounts_rl_sales_order_line_items_1"] = array (
  'name' => 'accounts_rl_sales_order_line_items_1',
  'type' => 'link',
  'relationship' => 'accounts_rl_sales_order_line_items_1',
  'source' => 'non-db',
  'module' => 'RL_Sales_Order_Line_Items',
  'bean_name' => 'RL_Sales_Order_Line_Items',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_RL_SALES_ORDER_LINE_ITEMS_1_FROM_RL_SALES_ORDER_LINE_ITEMS_TITLE',
);
