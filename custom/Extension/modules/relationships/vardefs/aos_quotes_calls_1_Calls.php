<?php
// created: 2014-08-26 09:34:04
$dictionary["Call"]["fields"]["aos_quotes_calls_1"] = array (
  'name' => 'aos_quotes_calls_1',
  'type' => 'link',
  'relationship' => 'aos_quotes_calls_1',
  'source' => 'non-db',
  'module' => 'AOS_Quotes',
  'bean_name' => 'AOS_Quotes',
  'vname' => 'LBL_AOS_QUOTES_CALLS_1_FROM_AOS_QUOTES_TITLE',
  'id_name' => 'aos_quotes_calls_1aos_quotes_ida',
);
$dictionary["Call"]["fields"]["aos_quotes_calls_1_name"] = array (
  'name' => 'aos_quotes_calls_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_AOS_QUOTES_CALLS_1_FROM_AOS_QUOTES_TITLE',
  'save' => true,
  'id_name' => 'aos_quotes_calls_1aos_quotes_ida',
  'link' => 'aos_quotes_calls_1',
  'table' => 'aos_quotes',
  'module' => 'AOS_Quotes',
  'rname' => 'name',
);
$dictionary["Call"]["fields"]["aos_quotes_calls_1aos_quotes_ida"] = array (
  'name' => 'aos_quotes_calls_1aos_quotes_ida',
  'type' => 'link',
  'relationship' => 'aos_quotes_calls_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_AOS_QUOTES_CALLS_1_FROM_CALLS_TITLE',
);
