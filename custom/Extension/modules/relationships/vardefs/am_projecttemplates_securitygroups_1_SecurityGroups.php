<?php
// created: 2016-04-25 17:24:12
$dictionary["SecurityGroup"]["fields"]["am_projecttemplates_securitygroups_1"] = array (
  'name' => 'am_projecttemplates_securitygroups_1',
  'type' => 'link',
  'relationship' => 'am_projecttemplates_securitygroups_1',
  'source' => 'non-db',
  'module' => 'AM_ProjectTemplates',
  'bean_name' => 'AM_ProjectTemplates',
  'vname' => 'LBL_AM_PROJECTTEMPLATES_SECURITYGROUPS_1_FROM_AM_PROJECTTEMPLATES_TITLE',
);
