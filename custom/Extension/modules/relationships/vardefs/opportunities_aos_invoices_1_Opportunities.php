<?php
// created: 2014-08-19 18:08:45
$dictionary["Opportunity"]["fields"]["opportunities_aos_invoices_1"] = array (
  'name' => 'opportunities_aos_invoices_1',
  'type' => 'link',
  'relationship' => 'opportunities_aos_invoices_1',
  'source' => 'non-db',
  'module' => 'AOS_Invoices',
  'bean_name' => 'AOS_Invoices',
  'side' => 'right',
  'vname' => 'LBL_OPPORTUNITIES_AOS_INVOICES_1_FROM_AOS_INVOICES_TITLE',
);
