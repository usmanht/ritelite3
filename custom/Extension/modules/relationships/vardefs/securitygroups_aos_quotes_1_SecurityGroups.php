<?php
// created: 2014-05-02 12:12:37
$dictionary["SecurityGroup"]["fields"]["securitygroups_aos_quotes_1"] = array (
  'name' => 'securitygroups_aos_quotes_1',
  'type' => 'link',
  'relationship' => 'securitygroups_aos_quotes_1',
  'source' => 'non-db',
  'module' => 'AOS_Quotes',
  'bean_name' => 'AOS_Quotes',
  'vname' => 'LBL_SECURITYGROUPS_AOS_QUOTES_1_FROM_AOS_QUOTES_TITLE',
);
