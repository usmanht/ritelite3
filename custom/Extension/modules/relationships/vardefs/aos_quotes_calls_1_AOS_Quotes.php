<?php
// created: 2014-08-26 09:34:04
$dictionary["AOS_Quotes"]["fields"]["aos_quotes_calls_1"] = array (
  'name' => 'aos_quotes_calls_1',
  'type' => 'link',
  'relationship' => 'aos_quotes_calls_1',
  'source' => 'non-db',
  'module' => 'Calls',
  'bean_name' => 'Call',
  'side' => 'right',
  'vname' => 'LBL_AOS_QUOTES_CALLS_1_FROM_CALLS_TITLE',
);
