<?php
// created: 2017-10-06 11:09:43
$dictionary["AOS_Products"]["fields"]["aos_products_cases_1"] = array (
  'name' => 'aos_products_cases_1',
  'type' => 'link',
  'relationship' => 'aos_products_cases_1',
  'source' => 'non-db',
  'module' => 'Cases',
  'bean_name' => 'Case',
  'side' => 'right',
  'vname' => 'LBL_AOS_PRODUCTS_CASES_1_FROM_CASES_TITLE',
);
