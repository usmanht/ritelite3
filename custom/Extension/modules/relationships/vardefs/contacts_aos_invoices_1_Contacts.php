<?php
// created: 2014-05-07 16:27:46
$dictionary["Contact"]["fields"]["contacts_aos_invoices_1"] = array (
  'name' => 'contacts_aos_invoices_1',
  'type' => 'link',
  'relationship' => 'contacts_aos_invoices_1',
  'source' => 'non-db',
  'module' => 'AOS_Invoices',
  'bean_name' => 'AOS_Invoices',
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_AOS_INVOICES_1_FROM_AOS_INVOICES_TITLE',
);
