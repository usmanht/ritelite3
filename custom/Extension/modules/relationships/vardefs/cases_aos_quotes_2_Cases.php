<?php
// created: 2017-10-05 08:42:03
$dictionary["Case"]["fields"]["cases_aos_quotes_2"] = array (
  'name' => 'cases_aos_quotes_2',
  'type' => 'link',
  'relationship' => 'cases_aos_quotes_2',
  'source' => 'non-db',
  'module' => 'AOS_Quotes',
  'bean_name' => 'AOS_Quotes',
  'side' => 'right',
  'vname' => 'LBL_CASES_AOS_QUOTES_2_FROM_AOS_QUOTES_TITLE',
);
