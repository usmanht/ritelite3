<?php
 // created: 2014-05-07 16:23:44
$layout_defs["Contacts"]["subpanel_setup"]['contacts_rl_sales_order_line_items_2'] = array (
  'order' => 100,
  'module' => 'RL_Sales_Order_Line_Items',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CONTACTS_RL_SALES_ORDER_LINE_ITEMS_2_FROM_RL_SALES_ORDER_LINE_ITEMS_TITLE',
  'get_subpanel_data' => 'contacts_rl_sales_order_line_items_2',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
