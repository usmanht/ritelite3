<?php
 // created: 2018-01-11 10:37:23
$layout_defs["AOS_Product_Categories"]["subpanel_setup"]['aos_product_categories_bugs_1'] = array (
  'order' => 100,
  'module' => 'Bugs',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_AOS_PRODUCT_CATEGORIES_BUGS_1_FROM_BUGS_TITLE',
  'get_subpanel_data' => 'aos_product_categories_bugs_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
