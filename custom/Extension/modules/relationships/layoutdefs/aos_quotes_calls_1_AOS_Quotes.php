<?php
 // created: 2014-08-26 09:34:04
$layout_defs["AOS_Quotes"]["subpanel_setup"]['aos_quotes_calls_1'] = array (
  'order' => 100,
  'module' => 'Calls',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_AOS_QUOTES_CALLS_1_FROM_CALLS_TITLE',
  'get_subpanel_data' => 'aos_quotes_calls_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
