<?php
 // created: 2015-02-16 16:56:38
$layout_defs["SecurityGroups"]["subpanel_setup"]['timesheet_securitygroups_1'] = array (
  'order' => 100,
  'module' => 'Timesheet',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_TIMESHEET_SECURITYGROUPS_1_FROM_TIMESHEET_TITLE',
  'get_subpanel_data' => 'timesheet_securitygroups_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
