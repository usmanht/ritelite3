<?php
// created: 2015-01-12 15:28:24
$dictionary["SecurityGroup"]["fields"]["aos_invoices_securitygroups_1"] = array (
  'name' => 'aos_invoices_securitygroups_1',
  'type' => 'link',
  'relationship' => 'aos_invoices_securitygroups_1',
  'source' => 'non-db',
  'module' => 'AOS_Invoices',
  'bean_name' => 'AOS_Invoices',
  'vname' => 'LBL_AOS_INVOICES_SECURITYGROUPS_1_FROM_AOS_INVOICES_TITLE',
);
