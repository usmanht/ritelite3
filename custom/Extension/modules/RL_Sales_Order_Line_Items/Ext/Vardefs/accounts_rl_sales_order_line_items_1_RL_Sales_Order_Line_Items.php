<?php
// created: 2014-05-07 16:09:41
$dictionary["RL_Sales_Order_Line_Items"]["fields"]["accounts_rl_sales_order_line_items_1"] = array (
  'name' => 'accounts_rl_sales_order_line_items_1',
  'type' => 'link',
  'relationship' => 'accounts_rl_sales_order_line_items_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_ACCOUNTS_RL_SALES_ORDER_LINE_ITEMS_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_rl_sales_order_line_items_1accounts_ida',
);
$dictionary["RL_Sales_Order_Line_Items"]["fields"]["accounts_rl_sales_order_line_items_1_name"] = array (
  'name' => 'accounts_rl_sales_order_line_items_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_RL_SALES_ORDER_LINE_ITEMS_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_rl_sales_order_line_items_1accounts_ida',
  'link' => 'accounts_rl_sales_order_line_items_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["RL_Sales_Order_Line_Items"]["fields"]["accounts_rl_sales_order_line_items_1accounts_ida"] = array (
  'name' => 'accounts_rl_sales_order_line_items_1accounts_ida',
  'type' => 'link',
  'relationship' => 'accounts_rl_sales_order_line_items_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_RL_SALES_ORDER_LINE_ITEMS_1_FROM_RL_SALES_ORDER_LINE_ITEMS_TITLE',
);
