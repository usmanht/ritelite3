<?php
// created: 2014-08-26 12:19:38
$dictionary["Call"]["fields"]["opportunities_calls_1"] = array (
  'name' => 'opportunities_calls_1',
  'type' => 'link',
  'relationship' => 'opportunities_calls_1',
  'source' => 'non-db',
  'module' => 'Opportunities',
  'bean_name' => 'Opportunity',
  'vname' => 'LBL_OPPORTUNITIES_CALLS_1_FROM_OPPORTUNITIES_TITLE',
  'id_name' => 'opportunities_calls_1opportunities_ida',
);
$dictionary["Call"]["fields"]["opportunities_calls_1_name"] = array (
  'name' => 'opportunities_calls_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_OPPORTUNITIES_CALLS_1_FROM_OPPORTUNITIES_TITLE',
  'save' => true,
  'id_name' => 'opportunities_calls_1opportunities_ida',
  'link' => 'opportunities_calls_1',
  'table' => 'opportunities',
  'module' => 'Opportunities',
  'rname' => 'name',
);
$dictionary["Call"]["fields"]["opportunities_calls_1opportunities_ida"] = array (
  'name' => 'opportunities_calls_1opportunities_ida',
  'type' => 'link',
  'relationship' => 'opportunities_calls_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_OPPORTUNITIES_CALLS_1_FROM_CALLS_TITLE',
);
