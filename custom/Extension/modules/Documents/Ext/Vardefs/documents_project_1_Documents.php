<?php
// created: 2014-04-05 18:16:31
$dictionary["Document"]["fields"]["documents_project_1"] = array (
  'name' => 'documents_project_1',
  'type' => 'link',
  'relationship' => 'documents_project_1',
  'source' => 'non-db',
  'module' => 'Project',
  'bean_name' => 'Project',
  'vname' => 'LBL_DOCUMENTS_PROJECT_1_FROM_PROJECT_TITLE',
);
