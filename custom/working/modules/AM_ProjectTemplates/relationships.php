<?php
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

$relationships = array (
  'am_projecttemplates_modified_user' => 
  array (
    'id' => '684ad858-e5b6-e002-5726-5718a51a0c0e',
    'relationship_name' => 'am_projecttemplates_modified_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'AM_ProjectTemplates',
    'rhs_table' => 'am_projecttemplates',
    'rhs_key' => 'modified_user_id',
    'join_table' => NULL,
    'join_key_lhs' => NULL,
    'join_key_rhs' => NULL,
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => NULL,
    'relationship_role_column_value' => NULL,
    'reverse' => 0,
    'deleted' => 0,
    'readonly' => true,
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => true,
  ),
  'am_projecttemplates_created_by' => 
  array (
    'id' => '68cef7c7-f887-4456-5a15-5718a5fb0953',
    'relationship_name' => 'am_projecttemplates_created_by',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'AM_ProjectTemplates',
    'rhs_table' => 'am_projecttemplates',
    'rhs_key' => 'created_by',
    'join_table' => NULL,
    'join_key_lhs' => NULL,
    'join_key_rhs' => NULL,
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => NULL,
    'relationship_role_column_value' => NULL,
    'reverse' => 0,
    'deleted' => 0,
    'readonly' => true,
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => true,
  ),
  'am_projecttemplates_assigned_user' => 
  array (
    'id' => '6951ad56-b20c-c0b7-fcae-5718a574d2f0',
    'relationship_name' => 'am_projecttemplates_assigned_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'AM_ProjectTemplates',
    'rhs_table' => 'am_projecttemplates',
    'rhs_key' => 'assigned_user_id',
    'join_table' => NULL,
    'join_key_lhs' => NULL,
    'join_key_rhs' => NULL,
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => NULL,
    'relationship_role_column_value' => NULL,
    'reverse' => 0,
    'deleted' => 0,
    'readonly' => true,
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => true,
  ),
  'am_projecttemplates_project_1' => 
  array (
    'id' => 'e56dae97-2b13-3cce-1f1f-5718a5ba52bd',
    'relationship_name' => 'am_projecttemplates_project_1',
    'lhs_module' => 'AM_ProjectTemplates',
    'lhs_table' => 'am_projecttemplates',
    'lhs_key' => 'id',
    'rhs_module' => 'Project',
    'rhs_table' => 'project',
    'rhs_key' => 'id',
    'join_table' => 'am_projecttemplates_project_1_c',
    'join_key_lhs' => 'am_projecttemplates_project_1am_projecttemplates_ida',
    'join_key_rhs' => 'am_projecttemplates_project_1project_idb',
    'relationship_type' => 'many-to-many',
    'relationship_role_column' => NULL,
    'relationship_role_column_value' => NULL,
    'reverse' => 0,
    'deleted' => 0,
    'readonly' => true,
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => true,
  ),
  'am_tasktemplates_am_projecttemplates' => 
  array (
    'id' => 'e5f52b51-565f-1ec0-e0a9-5718a50a754a',
    'relationship_name' => 'am_tasktemplates_am_projecttemplates',
    'lhs_module' => 'AM_ProjectTemplates',
    'lhs_table' => 'am_projecttemplates',
    'lhs_key' => 'id',
    'rhs_module' => 'AM_TaskTemplates',
    'rhs_table' => 'am_tasktemplates',
    'rhs_key' => 'id',
    'join_table' => 'am_tasktemplates_am_projecttemplates_c',
    'join_key_lhs' => 'am_tasktemplates_am_projecttemplatesam_projecttemplates_ida',
    'join_key_rhs' => 'am_tasktemplates_am_projecttemplatesam_tasktemplates_idb',
    'relationship_type' => 'many-to-many',
    'relationship_role_column' => NULL,
    'relationship_role_column_value' => NULL,
    'reverse' => 0,
    'deleted' => 0,
    'readonly' => true,
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => true,
  ),
  'am_projecttemplates_securitygroups_1' => 
  array (
    'lhs_module' => 'AM_ProjectTemplates',
    'relationship_type' => 'many-to-many',
    'rhs_module' => 'SecurityGroups',
    'lhs_label' => 'Project Templates',
    'rhs_label' => 'Security Groups Management',
    'lhs_subpanel' => 'default',
    'rhs_subpanel' => 'default',
    'readonly' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => true,
    'relationship_name' => 'am_projecttemplates_securitygroups_1',
  ),
);